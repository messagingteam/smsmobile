import 'dart:convert';
import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/routing/router.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String user = prefs.getString('user');
  String initpage = login;
  if (user != null) {
    Map<String, dynamic> parsed_user = jsonDecode(user);
    token = parsed_user["token"];
    initpage = splash;
  }
  runApp(MyApp(initpage));
}

class MyApp extends StatelessWidget {
  final String firstpage;
  MyApp(this.firstpage);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserProvider()),
      ],
      child: MaterialApp(
        title: 'EaziSend',
        theme: ThemeData(
          fontFamily: 'Avenir',
        ),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: generateRoute,
        initialRoute: firstpage,
      ),
    );
  }
}
