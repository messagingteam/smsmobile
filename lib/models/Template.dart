class Template {
  String id;
  String title;
  String message;
  String created_at;
  bool selectable = false;

  Template({this.message, this.title});

  Template.Create(Map<String, dynamic> json) {
    this.id = json["id"];
    this.title = json["title"];
    this.message = json["message"];
    this.created_at = json["created_at"];
  }
}
