class Group {
  String id;
  String name;
  String user_id;
  String description;
  bool selectable = false;

  Group({this.id, this.name, this.user_id, this.description});
  Group.Create(Map<String, dynamic> json) {
    this.id = json["id"];
    this.name = json["name"];
    this.user_id = json["user_id"];
    this.description = json["description"];
  }
}
