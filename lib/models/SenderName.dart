class SenderName {
  String id;
  String sender_name;

  SenderName({this.id, this.sender_name});
  SenderName.Create(Map<String, dynamic> json) {
    this.id = json["id"];
    this.sender_name = json["sender_name"];
  }
}
