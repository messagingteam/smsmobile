import 'package:eazisend/models/Contact.dart';

class ContactsPaginator {
  int currentPage;
  List<Contact> contacts = [];
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;
  String errorMessage;

  ContactsPaginator.withError(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  ContactsPaginator.fromJson(Map<String, dynamic> json) {
    this.currentPage = json["current_page"];
    json["data"].forEach((item) {
      this.contacts.add(Contact.Create(item));
    });
    this.firstPageUrl = json["first_page_url"];
    this.from = json["from"];
    this.lastPage = json["last_page"];
    this.lastPageUrl = json["last_page_url"];
    this.nextPageUrl = json["next_page_url"];
    this.path = json["path"];
    this.perPage = json["per_page"];
    this.prevPageUrl = json["prev_page_url"];
    this.to = json["to"];
    this.total = json["total"];
  }
}
