import 'Message.dart';

class Campaign {
  String campaignDate;
  String campaignId;
  int failed;
  int pending;
  int sent;
  String status;
  int total;
  List<Message> messages;

  Campaign.Create(Map<String, dynamic> json) {
    this.campaignDate = json["campaignDate"];
    this.campaignId = json["campaignId"];
    this.failed = json["failed"];
    this.pending = json["pending"];
    this.sent = json["sent"];
    this.status = json["status"];
    this.total = json["total"];
    if (json["messages"] != null) {
      this.messages = json["messages"].map((e) => Message.Create(e)).toList();
    }
  }
}
