import 'package:eazisend/models/Group.dart';

class GroupsPaginator {
  int currentPage;
  List<Group> groups = [];
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;
  String errorMessage;

  GroupsPaginator.withError(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  GroupsPaginator.fromJson(Map<String, dynamic> json) {
    this.currentPage = json["current_page"];
    json["data"].forEach((item) {
      this.groups.add(Group.Create(item));
    });
    print(json["per_page"]);
    this.firstPageUrl = json["first_page_url"];
    this.from = json["from"];
    this.lastPage = json["last_page"];
    this.lastPageUrl = json["last_page_url"];
    this.nextPageUrl = json["next_page_url"];
    this.path = json["path"];
    this.perPage = 10;
    this.to = json["to"];
    this.total = json["total"];
  }
}
