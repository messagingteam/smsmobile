class Packages {
  String id;
  String price;
  int quantity;
  String expires;
  int max_quantity;
  int min_quantity;
  String price_per_sms;

  Packages.Create(Map<String, dynamic> json) {
    this.id = json["id"];
    this.price = json["price"];
    this.quantity = json["quantity"];
    this.expires = json["expires"];
    this.max_quantity = json["max_quantity"];
    this.min_quantity = json["min_quantity"];
    this.price_per_sms = json["price_per_sms"];
  }
}
