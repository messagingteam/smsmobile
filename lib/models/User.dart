import 'dart:convert';

import 'package:eazisend/resources/AuthRepo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User {
  String token;
  String userId;
  String last_name;
  String other_names;
  String password;
  String email;
  String phone;

  User({this.token, this.userId, this.last_name, this.other_names});
  setPassword(String password) {
    this.password = password;
  }

  User.Create(Map<String, dynamic> json) {
    this.token = json["token"];
    this.userId = json["userId"];
    this.last_name = json["last_name"];
    this.other_names = json["other_names"];
    this.email = json["email"];
    this.phone = json["phone"];
  }

  convertToMap() {
    return {
      "token": this.token,
      "userId": this.userId,
      "last_name": this.last_name,
      "other_names": this.other_names,
      "email": this.email,
      "phone": this.phone
    };
  }

  fullname() {
    return this.last_name + ' ' + this.other_names;
  }

  save() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user", jsonEncode(this.convertToMap()));
  }

  Future<dynamic> login(String email, String password) async {
    Map<String, dynamic> response =
        await AuthRepo().login(email: email, password: password);
    if (response["status"] == 1) {
      Map<String, dynamic> userData = response["data"];
      this.token = userData["token"];
      this.userId = userData["userId"];
      this.last_name = userData["last_name"];
      this.other_names = userData["other_names"];
      return this;
    }
    return response;
  }
}
