import 'package:eazisend/resources/ContactRepo.dart';

import 'Group.dart';

class Contact {
  String id;
  String user_id;
  String phone;
  String title;
  String surname;
  String othernames;
  String dob;
  bool selected = false;

  fullname() {
    //return 'hello';
    String _surname = this.surname ?? '';
    String _othernames = this.othernames ?? '';
    return _surname + ' ' + _othernames;
  }

  Contact(
      {this.id,
      this.user_id,
      this.phone,
      this.title,
      this.surname,
      this.othernames,
      this.dob});

  Contact.Create(Map<String, dynamic> json) {
    this.id = json["id"];
    this.user_id = json["user_id"];
    this.phone = json["phone"];
    this.title = json["title"];
    this.surname = json["surname"];
    this.othernames = json["othernames"];
    this.dob = json["dob"];
  }

  Future<String> save() async {
    Map response = await ContactRepo().save(this);
    if (response["status"] == 1) {
      return response["message"];
    }
    return response["message"];
  }

  Future<String> update() async {
    Map response = await ContactRepo().updateContact(this);
    if (response["status"] == 1) {
      return response["message"];
    }
    return response["message"];
  }

  Future<String> delete() async {
    Map response = await ContactRepo().deleteContact(this);
    if (response["status"] == 1) {
      return response["message"];
    }
    return response["message"];
  }

  Future<String> addToGroup(Group group) async {
    Map response = await ContactRepo().addContactToGroup(group, this);
    if (response["status"] == 1) {
      return response["message"];
    }
    return response["message"];
  }

  Future<String> removeFromGroup(Group group) async {
    Map response = await ContactRepo().removeContactFromGroup(group, this);
    if (response["status"] == 1) {
      return response["message"];
    }
    return response["message"];
  }
}
