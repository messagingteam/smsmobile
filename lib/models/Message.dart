class Message {
  String contact;
  String failed_reason;
  String message;
  String phone;
  String schedule_date;
  String sender_id;
  String status;
  String token;

  Message.Create(Map<String, dynamic> json) {
    this.contact = json["contact"];
    this.failed_reason = json["failed_reason"];
    this.message = json["message"];
    this.phone = json["phone"];
    this.schedule_date = json["schedule_date"];
    this.sender_id = json["sender_id"];
    this.status = json["status"];
    this.token = json["token"];
  }
}
