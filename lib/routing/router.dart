import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/screens/About.dart';
import 'package:eazisend/screens/Account.dart';
import 'package:eazisend/screens/AddNewContact.dart';
import 'package:eazisend/screens/AddNewGroup.dart';
import 'package:eazisend/screens/AddNewTemplate.dart';
import 'package:eazisend/widgets/CampaignProgress.dart';
import 'package:eazisend/screens/Contacts.dart';
import 'package:eazisend/screens/Groups.dart';
import 'package:eazisend/screens/Help.dart';
import 'package:eazisend/screens/Home.dart';
import 'package:eazisend/screens/Login.dart';
import 'package:eazisend/screens/History.dart';
import 'package:eazisend/screens/MessageTemplates.dart';
import 'package:eazisend/screens/Otp.dart';
import 'package:eazisend/screens/Packages.dart';
import 'package:eazisend/screens/Register.dart';
import 'package:eazisend/screens/SelectableContactList.dart';
import 'package:eazisend/screens/SelectableGroupList.dart';
import 'package:eazisend/screens/SelectablePhoneContacts.dart';
import 'package:eazisend/screens/SelectableTemplates.dart';
import 'package:eazisend/screens/SendMessage.dart';
import 'package:eazisend/screens/SendTo.dart';
import 'package:eazisend/screens/SetPassword.dart';
import 'package:eazisend/screens/SplashScreen.dart';
import 'package:eazisend/screens/TopUp.dart';
import 'package:eazisend/screens/UpdateProfile.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case home:
      return MaterialPageRoute(builder: (context) => Home());
      break;
    case login:
      return MaterialPageRoute(builder: (context) => Login());
      break;
    case register:
      return MaterialPageRoute(builder: (context) => Register());
      break;
    case setupPassword:
      return MaterialPageRoute(builder: (context) => SetPassword());
      break;
    case otp:
      final Map<String, dynamic> args = settings.arguments;
      return MaterialPageRoute(
          builder: (context) =>
              Otp(token: args["token"], phone: args["phone"]));
      break;
    case contacts:
      return MaterialPageRoute(builder: (context) => Contacts());
      break;
    case groups:
      return MaterialPageRoute(builder: (context) => Groups());
      break;
    case addnewcontact:
      return MaterialPageRoute(builder: (context) => AddNewContact());
      break;
    case addnewgroup:
      return MaterialPageRoute(builder: (context) => AddNewGroup());
      break;
    case sendmessage:
      final Map<String, dynamic> args = settings.arguments;
      return MaterialPageRoute(
          builder: (context) => SendMessage(
                sendtype: args["sendtype"],
              ));
      break;
    case account:
      return MaterialPageRoute(builder: (context) => Account());
      break;
    case history:
      return MaterialPageRoute(builder: (context) => History());
      break;
    case list:
      return MaterialPageRoute(builder: (context) => SelectableGroupList());
      break;
    case templates:
      return MaterialPageRoute(builder: (context) => MessageTemplates());
      break;
    case addnewtemplate:
      return MaterialPageRoute(builder: (context) => AddNewTemplate());
      break;
    case splash:
      return MaterialPageRoute(builder: (context) => SplashScreen());
      break;
    case sendto:
      return MaterialPageRoute(builder: (context) => SendTo());
      break;
    case phonecontacts:
      return MaterialPageRoute(builder: (context) => SelectablePhoneContacts());
      break;
    case help:
      return MaterialPageRoute(builder: (context) => Help());
      break;
    case about:
      return MaterialPageRoute(builder: (context) => About());
      break;
    case packages:
      return MaterialPageRoute(builder: (context) => PackagesPage());
      break;
    case topup:
      return MaterialPageRoute(builder: (context) => TopUp());
      break;
    case selecttemplate:
      return MaterialPageRoute(builder: (context) => SelectableTemplate());
      break;
    case campaignprogress:
      return MaterialPageRoute(builder: (context) => CampaignProgress());
      break;
  }
}
