import 'package:eazisend/models/ContactsPaginator.dart';
import 'package:eazisend/resources/ContactRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/screens/UpdateContact.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/ContactTile.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/cupertino.dart';
import 'package:eazisend/models/Contact.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_paginator/flutter_paginator.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class Contacts extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ContactsState();
  }
}

class ContactsState extends State<Contacts> {
  List<Contact> contacts = [];
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0,
            expandedHeight: 100.0,
            backgroundColor: app_color,
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              title: Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.contact_page,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Contacts',
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            actions: <Widget>[
              if (contacts.any((e) => e.selected))
                Row(
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        CupertinoIcons.person_2_square_stack,
                        color: Colors.white,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        deleteBulkContact(context);
                      },
                      icon: Icon(
                        CupertinoIcons.delete_simple,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, addnewcontact);
                },
                icon: Icon(
                  CupertinoIcons.add_circled,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  height: 30,
                  color: app_color,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Center(
                        child: Text(
                      "Swipe contact for more options",
                      style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.w900,
                          fontSize: 16),
                    )),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            topLeft: Radius.circular(40))),
                  ),
                ),
                Divider(),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 250,
                  child: Paginator.listView(
                    key: paginatorGlobalKey,
                    pageLoadFuture: sendPagesDataRequest,
                    pageItemsGetter: listItemsGetterPages,
                    listItemBuilder: listItemBuilder,
                    loadingWidgetBuilder: loadingWidgetMaker,
                    errorWidgetBuilder: errorWidgetMaker,
                    emptyListWidgetBuilder: emptyListWidgetMaker,
                    totalItemsGetter: totalPagesGetter,
                    pageErrorChecker: pageErrorChecker,
                    scrollPhysics: BouncingScrollPhysics(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<ContactsPaginator> sendPagesDataRequest(int page) async {
    print('page ${page}');
    try {
      Map response = await ContactRepo().fetchContacts(page.toString());
      ContactsPaginator pagesData =
          ContactsPaginator.fromJson(response["data"]);
      return pagesData;
    } catch (e) {
      print("check");
      print(e.toString());
      return ContactsPaginator.withError("Oops.. Fetching contacts failed");
    }
  }

  List<Contact> listItemsGetterPages(ContactsPaginator pagesData) {
    // return pagesData.products;
    pagesData.contacts.forEach((value) {
      contacts.add(value);
    });
    print("itemgetter");
    return contacts;
  }

  Widget listItemBuilder(dynamic contact, int index) {
    print('init' + index.toString());
    print(contact);
    return Column(
      children: [
        ContactTile(
          contact,
          index,
          onChecked: (value) {
            setState(() {
              contacts[index].selected = value;
            });
          },
          onDeleteTap: () {
            deleteContact(context, contact, index);
          },
          onEditTap: () {
            editContact(context, contact, index);
          },
          onAddToGroupTap: null,
        ),
        Divider()
      ],
    );
  }

  Widget loadingWidgetMaker() {
    return Center(child: CircularProgressIndicator());
  }

  Widget errorWidgetMaker(ContactsPaginator pagesData, retryListener) {
    return Retry(
      buttontext: "Retry",
      icon: Icons.refresh,
      errormessage: pagesData.errorMessage ?? "No contacts Found",
      tryAgain: () {
        retryListener();
      },
    );
  }

  Widget emptyListWidgetMaker(ContactsPaginator pagesData) {
    return Center(
      child: Text(
        'No contacts found',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      ),
    );
  }

  int totalPagesGetter(ContactsPaginator pagesData) {
    print('lastpage:' + pagesData.lastPage.toString());
    return pagesData.total;
  }

  bool pageErrorChecker(ContactsPaginator pagesData) {
    if (pagesData.errorMessage != null) return true;
    if (pagesData.currentPage == 1 && pagesData.contacts.length == 0)
      return true;
    return false;
  }

  editContact(BuildContext context, Contact contact, int index) async {
    final response = Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpdateContact(contact),
      ),
    );
  }

  deleteContact(BuildContext context, Contact contact, int index) async {
    SureToDelete(context, 'Alert', 'Are you sure you want to delete', () async {
      Map response = await ContactRepo().deleteContact(contact);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(response["message"] ?? ''),
        ),
      );
      if (response["status"] == 1) {
        setState(() {
          contacts.removeAt(index);
        });
      }
    });
  }

  addSelectedContactToGroup(BuildContext context) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text("Selected contacts added to group"),
      ),
    );
  }

  deleteBulkContact(BuildContext context) {
    SureToDelete(
        context, 'Alert', 'Are you sure you want to delete selected contacts',
        () async {
      Map response = await ContactRepo().deleteContacts(
          contacts.where((e) => e.selected).cast<Contact>().toList());
      print(response);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(response["message"] ?? ''),
        ),
      );
      if (response["status"] == 1) {
        setState(() {
          contacts.removeWhere((e) => e.selected);
        });
      }
    });
  }
}
