import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SendTo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                elevation: 0,
                expandedHeight: 150.0,
                backgroundColor: app_color,
                flexibleSpace: FlexibleSpaceBar(
                  stretchModes: <StretchMode>[
                    StretchMode.zoomBackground,
                    StretchMode.blurBackground,
                    StretchMode.fadeTitle,
                  ],
                  title: Row(
                    children: [
                      SizedBox(
                        width: 15,
                      ),
                      Icon(
                        Icons.contact_page,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Send Message',
                              style: TextStyle(
                                  fontFamily: "Avenir",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    Container(
                      height: 30,
                      color: app_color,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(40),
                                topLeft: Radius.circular(40))),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'instant'});
                            },
                            title: Text("Instant Single Message"),
                            subtitle: Text("Send instant single message"),
                            leading: Icon(CupertinoIcons.bubble_left),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'list'});
                            },
                            title: Text("List of Contacts"),
                            subtitle: Text(
                                "Send message to list of contacts separated by commas"),
                            leading: Icon(CupertinoIcons.bubble_left),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'phone'});
                            },
                            title: Text("To Phone Contacts"),
                            subtitle:
                                Text("Send messages to your phone contacts"),
                            leading: Icon(CupertinoIcons.bubble_left),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'contact'});
                            },
                            title: Text("To Saved Contact"),
                            subtitle: Text(
                                "Send messages to contacts saved on Eazisend clouds"),
                            leading: Icon(CupertinoIcons.bubble_left),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'group'});
                            },
                            title: Text("To Group"),
                            subtitle: Text("Send messages to a group"),
                            leading: Icon(CupertinoIcons.person_2_square_stack),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          ListTile(
                            onTap: () {
                              Navigator.pushNamed(context, sendmessage,
                                  arguments: {"sendtype": 'excel'});
                            },
                            title: Text("From Excel"),
                            subtitle:
                                Text("Send messages to contacts in excel file"),
                            leading: Icon(CupertinoIcons.doc_text),
                            trailing: Icon(CupertinoIcons.chevron_forward),
                          ),
                          Divider(),
                          // ListTile(
                          //   title: Text("Snapshot"),
                          //   subtitle: Text(
                          //       "Send messages to contacts by taking snapshot of the list of numbers"),
                          //   leading: Icon(CupertinoIcons.camera),
                          //   trailing: Icon(CupertinoIcons.chevron_forward),
                          // ),
                          // Divider(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
