import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SetPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SetPasswordState();
  }
}

class SetPasswordState extends State<SetPassword> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController passwordController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 40, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/profile.png',
                        width: 120,
                        height: 120,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 10, left: 20),
                      child: Text(
                        "Setup password",
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                          labelText: 'Password',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter your password';
                          }
                          if (value.length <= 5) {
                            return 'Password must be at least 6 characters';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Confirm password',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter your confirm password';
                          }
                          if (value != passwordController.text) {
                            return 'Enter same password';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            finish(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Finish",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Positioned(
          //   child: IconButton(
          //       icon: Icon(
          //         Icons.arrow_back,
          //         color: Colors.black,
          //       ),
          //       onPressed: () {
          //         Navigator.pop(context);
          //       }),
          //   top: 30,
          //   left: 10,
          // )
        ],
      ),
    );
  }

  finish(BuildContext context) async {
    //  perform operations
    final pr = await showLoading(context, "Please wait...");
    Map response = await AuthRepo().setPassword(
        password: passwordController.text,
        password_confirmation: passwordController.text);
    UserProvider provide = Provider.of<UserProvider>(context, listen: false);
    User user = provide.user;
    if (user != null) {
      user.setPassword(passwordController.text);
    }
    provide.user = user;
    Navigator.pushNamedAndRemoveUntil(context, home, (route) => false);
  }
}
