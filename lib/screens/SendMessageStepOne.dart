import 'package:eazisend/widgets/DropDown.dart';
import 'package:flutter/material.dart';

class SendMessageStepOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              padding: EdgeInsets.only(top: 40, bottom: 0, left: 20),
              child: Image.asset(
                'assets/images/login-register.png',
                width: 120,
                height: 120,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0, bottom: 2, left: 20),
              child: Text(
                "Send Message to ...",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
              child: TextFormField(
                initialValue: "Compose new Message",
                decoration: InputDecoration(
                  labelText: 'Choose Template',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
              child: TextFormField(
                maxLines: 3,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  labelText: 'Message',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
              child: TextFormField(
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  labelText: 'Sender Name',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
              child: TextFormField(
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  labelText: 'Choose Date',
                ),
              ),
            ),
            SizedBox(height: 40),
          ],
        ),
      ),
    );
  }
}
