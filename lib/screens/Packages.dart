import 'package:eazisend/models/Packages.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/PackagesRepo.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PackagesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PackagesState();
  }
}

class PackagesState extends State<PackagesPage> {
  bool isLoading = true;
  List<Packages> packages = [];
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Packages"),
        centerTitle: true,
        backgroundColor: app_color,
      ),
      body: FutureProvider<List<Packages>>(
        initialData: [],
        create: (context) => getPackages(),
        child: Consumer<List<Packages>>(
          builder: (context, myModel, child) {
            return ListView(
              children: [
                for (Packages p in myModel)
                  Column(
                    children: [
                      ListTile(
                        onTap: () {
                          BuyPackage(context, p);
                        },
                        leading: Icon(CupertinoIcons.cart),
                        title: Text(p.price),
                        subtitle: Text('SMS quantity ${p.quantity}'),
                        trailing: Icon(Icons.arrow_forward),
                      ),
                      Divider()
                    ],
                  )
              ],
            );
          },
        ),
      ),
    );
  }

  Future<List<Packages>> getPackages() async {
    List<Packages> getpackages = [];
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    if (provider.packages.isNotEmpty) {
      isLoading = false;
      getpackages = provider.packages;
    } else {
      Map response = await PackagesRepo().fetchPackages();
      if (response["status"] == 1) {
        response["data"].forEach((item) {
          getpackages.add(Packages.Create(item));
        });
        provider.packages = getpackages;
        isLoading = false;
      }
    }

    return getpackages;
  }

  BuyPackage(BuildContext context, Packages package) async {
    Confirm(context, 'Alert', 'Do you want to buy this package', () async {
      Map response = await PackagesRepo().BuyPackage(package);
      print(response);
      if (response["status"] == 1) {
        _launchUrl(response["data"]);
      } else {
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
      }
    });
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
