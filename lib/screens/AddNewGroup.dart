import 'package:eazisend/models/Group.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/GroupRepo.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddNewGroupState();
  }
}

class AddNewGroupState extends State<AddNewGroup> {
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController groupNameController = TextEditingController();
  final TextEditingController groupDescriptionController =
      TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 60, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/profile.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 2, left: 20),
                      child: Text(
                        "Add New Group",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: groupNameController,
                        decoration: InputDecoration(
                          labelText: 'Group name',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter group name';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        maxLines: 3,
                        style: TextStyle(color: Colors.white),
                        controller: groupDescriptionController,
                        decoration: InputDecoration(
                          labelText: 'Description',
                        ),
                      ),
                    ),
                    SizedBox(height: 40),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            addNewGroup(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Add New Group",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          )
        ],
      ),
    );
  }

  addNewGroup(BuildContext context) async {
    // final pr = await showLoading(context, 'Saving...');
    Group group = Group(
        name: groupNameController.text,
        description: groupDescriptionController.text);
    Map response = await GroupRepo().save(group);
    // await pr.hide();
    print(response);
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(response["message"])));
    Provider.of<UserProvider>(context, listen: false).addGroup(group);
  }
}
