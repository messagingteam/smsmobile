import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:code_fields/code_fields.dart';
import 'package:provider/provider.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';

class Otp extends StatefulWidget {
  String token;
  String phone;
  Otp({@required this.token, @required this.phone});
  @override
  State<StatefulWidget> createState() {
    return OtpState();
  }
}

class OtpState extends State<Otp> {
  final formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final CountdownController controller = CountdownController();
  String code;
  bool _isLoading = false;
  bool showcountdown = true;
  String resendText = 'Resend code';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Form(
              key: formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 40, bottom: 10, left: 20),
                        child: Image.asset(
                          'assets/images/profile.png',
                          width: 120,
                          height: 120,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, left: 20),
                        child: Text(
                          "OTP Verification",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Enter the 4-digit code sent to you at",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: '0241361156\t\t',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: app_color),
                                    ),
                                    TextSpan(
                                      text: ' Edit',
                                      style: TextStyle(
                                          fontSize: 18, color: app_color),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Center(
                              child: CodeFields(
                                  validator: (code) {
                                    if (code.length < 4) {
                                      return "Please complete the code";
                                    }
                                    return null;
                                  },
                                  autofocus: true,
                                  fieldHeight: 70,
                                  fieldWidth: 70,
                                  length: 4,
                                  textStyle: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  inputDecoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.grey.withOpacity(0.2),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                        borderSide: BorderSide(
                                            color: Colors.transparent)),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                  ),
                                  onCompleted: (value) {
                                    code = value.toString();
                                  },
                                  onChanged: (value) {
                                    code = value.toString();
                                  }),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: app_color_gradient),
                        ),
                        child: FlatButton(
                          color: Colors.transparent,
                          onPressed: () {
                            if (formKey.currentState.validate()) {
                              validateToken(context);
                              // formKey.currentState.save();
                            }
                          },
                          textColor: Colors.white,
                          child: Container(
                            height: 55,
                            padding: EdgeInsets.all(15),
                            child: Text(
                              "Continue",
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: showcountdown
                            ? null
                            : () {
                                resendToken(context);
                              },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              resendText,
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            if (showcountdown)
                              Countdown(
                                controller: controller,
                                seconds: 5,
                                build: (BuildContext context, double time) =>
                                    Text(
                                  'in ' + time.round().toString() + ' s',
                                  style: TextStyle(fontSize: 18),
                                ),
                                interval: Duration(milliseconds: 100),
                                onFinished: () {
                                  print('Timer is done!');
                                  setState(() {
                                    showcountdown = false;
                                  });
                                },
                              ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              top: 30,
              left: 10,
            )
          ],
        ));
  }

  validateToken(BuildContext context) async {
    var response =
        await AuthRepo().verifyOtp(token: widget.token, verificationCode: code);
    print(response);
    if (response["status"] == 1) {
      User user = User.Create(response["data"]);
      await user.save();
      Provider.of<UserProvider>(context, listen: false).user = user;
      Navigator.of(context)
          .pushNamedAndRemoveUntil(setupPassword, (route) => false);
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(response["message"])));
    }
  }

  resendToken(BuildContext context) async {
    setState(() {
      resendText = 'Resending...';
    });
    var response = await AuthRepo().resendToken(widget.phone);
    setState(() {
      resendText = 'Resend code';
    });
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(response["message"])));
    if (response["status"] == 1) {
      widget.token = response["token"];
      setState(() {
        showcountdown = true;
      });
    }
  }
}
