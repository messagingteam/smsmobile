import 'package:eazisend/models/Group.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/GroupRepo.dart';
import 'package:eazisend/resources/PackagesRepo.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/AppDropDown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class TopUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TopUpState();
  }
}

class TopUpState extends State<TopUp> {
  List<Map<String, String>> options = [
    {"name": "Price", "value": "price"},
    {"name": "Quantity", "value": "quantity"},
  ];
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController methodController =
      TextEditingController(text: 'price');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 60, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/profile.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 2, left: 20),
                      child: Text(
                        "Top Up Account",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text("Choose method of purchase "),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: AppDropdown(
                        title: "Select Option",
                        options: options,
                        valueSelected: (result) {
                          print(result);
                          methodController.text = result;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: amountController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Enter amount',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter a valid amount';
                          }
                          if (!_isNumeric(value)) {
                            return 'Enter a valid amount';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 40),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            buyNow(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Buy Now",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          )
        ],
      ),
    );
  }

  bool _isNumeric(String str) {
    if (str == null) {
      return false;
    }
    return double.tryParse(str) != null;
  }

  buyNow(BuildContext context) async {
    Map response = await PackagesRepo().priceEnquiry(
        method: methodController.text, quantity: amountController.text);
    print(response);
    if (response["status"] == 1) {
      Map<String, dynamic> data = response["data"];
      Confirm(context, "Info",
          "Price : ${data["cost"]} \n SMS Quantity : ${data["quantity"]} \n. Click to proceed ",
          () async {
        Map response = await PackagesRepo()
            .getPaymentLink(amount: data["quantity"].toString());
        print(response);
        if (response["status"] == 1) {
          _launchUrl(response["data"]);
        } else {
          _scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text(response["message"])));
        }
      }, confirmbtntext: "Buy Now", cancelbtntext: "Cancel");
    }
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
