import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/GroupsPaginator.dart';
import 'package:eazisend/resources/GroupRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/ContactTile.dart';
import 'package:eazisend/widgets/GroupTile.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paginator/flutter_paginator.dart';

import 'UpdateGroup.dart';

class Groups extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GroupsState();
  }
}

class GroupsState extends State<Groups> {
  List<Group> groups = [];
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0,
            expandedHeight: 100.0,
            backgroundColor: app_color,
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              title: Row(
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  Icon(
                    Icons.group,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Groups',
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            actions: <Widget>[
              if (groups.any((e) => e.selectable))
                IconButton(
                  onPressed: () {
                    deleteBulkGroup(context);
                  },
                  icon: Icon(
                    CupertinoIcons.delete_simple,
                    color: Colors.white,
                  ),
                ),
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, addnewgroup);
                },
                icon: Icon(
                  CupertinoIcons.add_circled,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  height: 30,
                  color: app_color,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Center(child: Text("Swipe group for more options")),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            topLeft: Radius.circular(40))),
                  ),
                ),
                Divider(),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 250,
                  child: Paginator.listView(
                    key: paginatorGlobalKey,
                    pageLoadFuture: sendPagesDataRequest,
                    pageItemsGetter: listItemsGetterPages,
                    listItemBuilder: listItemBuilder,
                    loadingWidgetBuilder: loadingWidgetMaker,
                    errorWidgetBuilder: errorWidgetMaker,
                    emptyListWidgetBuilder: emptyListWidgetMaker,
                    totalItemsGetter: totalPagesGetter,
                    pageErrorChecker: pageErrorChecker,
                    scrollPhysics: BouncingScrollPhysics(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<GroupsPaginator> sendPagesDataRequest(int page) async {
    print('page ${page}');
    // try {
    Map response = await GroupRepo().fetchGroups(page.toString());
    GroupsPaginator pagesData = GroupsPaginator.fromJson(response["data"]);
    return pagesData;
    // } catch (e) {
    //   print("check");
    //   print(e.toString());
    //   return GroupsPaginator.withError("Oops.. Fetching groups failed");
    // }
  }

  List<Group> listItemsGetterPages(GroupsPaginator pagesData) {
    // return pagesData.products;
    pagesData.groups.forEach((value) {
      groups.add(value);
    });
    print("itemgetter");
    return groups;
  }

  Widget listItemBuilder(dynamic group, int index) {
    print('init' + index.toString());
    return Column(
      children: [
        GroupTile(
          group,
          index,
          onChecked: (value) {
            setState(() {
              groups[index].selectable = value;
            });
          },
          onDeleteTap: () {
            deleteGroup(context, group, index);
          },
          onEditTap: () {
            editGroup(context, group, index);
          },
        ),
        Divider()
      ],
    );
  }

  Widget loadingWidgetMaker() {
    return Center(child: CircularProgressIndicator());
  }

  Widget errorWidgetMaker(GroupsPaginator pagesData, retryListener) {
    return Retry(
      buttontext: "Retry",
      icon: Icons.refresh,
      errormessage: pagesData.errorMessage ?? "No groups found",
      tryAgain: () {
        retryListener();
      },
    );
  }

  Widget emptyListWidgetMaker(GroupsPaginator pagesData) {
    return Center(
      child: Text(
        'No group Found',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      ),
    );
  }

  int totalPagesGetter(GroupsPaginator pagesData) {
    print('lastpage:' + pagesData.lastPage.toString());
    return pagesData.total;
  }

  bool pageErrorChecker(GroupsPaginator pagesData) {
    if (pagesData.errorMessage != null) return true;
    if (pagesData.currentPage == 1 && pagesData.groups.length == 0) return true;
    return false;
  }

  editGroup(BuildContext context, Group group, int index) async {
    final response = Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpdateGroup(group),
      ),
    );
  }

  deleteGroup(BuildContext context, Group group, int index) async {
    SureToDelete(context, 'Alert', 'Are you sure you want to delete', () async {
      Map response = await GroupRepo().deleteGroup(group);
      print(response);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(response["message"] ?? ''),
        ),
      );
      if (response["status"] == 1) {
        setState(() {
          groups.removeAt(index);
        });
      }
    });
  }

  deleteBulkGroup(BuildContext context) {
    SureToDelete(
        context, 'Alert', 'Are you sure you want to delete selected groups',
        () async {
      Map response = await GroupRepo().deleteGroups(
          groups.where((e) => e.selectable).cast<Group>().toList());
      print(response);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(response["message"] ?? ''),
        ),
      );
      if (response["status"] == 1) {
        setState(() {
          groups.removeWhere((e) => e.selectable);
        });
      }
    });
  }
}
