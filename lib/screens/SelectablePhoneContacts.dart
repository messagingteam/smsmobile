import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:eazisend/models/Contact.dart' as C;
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class SelectablePhoneContacts extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SelectablePhoneContactsState();
  }
}

class SelectablePhoneContactsState extends State<SelectablePhoneContacts> {
  List<C.Contact> _contacts = [];

  void initState() {
    super.initState();
    _askPermissions();
  }

  Future<void> _askPermissions() async {
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus != PermissionStatus.granted) {
      _handleInvalidPermissions(permissionStatus);
      print("here");
    } else {
      print("there");
      fetchContacts();
    }
  }

  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permission = await Permission.contacts.request();
      return permission;
    } else {
      return permission;
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to Contacts data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
      throw PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Access to Contacts data denied",
          details: null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Choose Contacts"),
        backgroundColor: app_color,
      ),
      body: SingleChildScrollView(
        child: _contacts.length == 0
            ? Center(
                child: Padding(
                  padding: const EdgeInsets.all(100.0),
                  child: CircularProgressIndicator(),
                ),
              )
            : Container(
                width: double.infinity,
                child: DataTable(dataRowHeight: 80, columns: const <DataColumn>[
                  DataColumn(
                    label: const Text('Select All'),
                  ),
                ], rows: [
                  for (C.Contact contact in _contacts)
                    DataRow(
                      cells: [
                        DataCell(
                          ListTile(
                            title: Text(contact.phone),
                            subtitle: Text(
                                contact.surname + ' ' + contact.othernames),
                          ),
                        )
                      ],
                      selected: contact.selected,
                      onSelectChanged: (bool value) {
                        setState(() {
                          contact.selected = value;
                        });
                      },
                    ),
                ]),
              ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context,
              _contacts.where((e) => e.selected).cast<C.Contact>().toList());
        },
        label: Text('Proceed'),
        icon: Icon(Icons.arrow_forward),
        backgroundColor: app_color,
      ),
    );
  }

  fetchContacts() async {
    List<C.Contact> _tempcontacts = [];
    Future.delayed(Duration(seconds: 2), () async {
      Iterable<Contact> contacts =
          await ContactsService.getContacts(withThumbnails: false);
      contacts.forEach((c) {
        c.phones.forEach((p) {
          _tempcontacts.add(C.Contact(
              surname: c.displayName, othernames: '', phone: p.value));
        });
      });
      setState(() {
        _contacts = _tempcontacts;
      });
    });
  }
}
