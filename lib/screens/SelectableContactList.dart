import 'package:eazisend/models/ContactsPaginator.dart';
import 'package:eazisend/resources/ContactRepo.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/ContactTile.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paginator/flutter_paginator.dart';
import 'package:eazisend/models/Contact.dart';

class SelectableContactList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SelectableContactListState();
  }
}

class SelectableContactListState extends State<SelectableContactList> {
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();
  List<Contact> contacts = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Choose Contacts"),
        backgroundColor: app_color,
      ),
      body: Paginator.listView(
        key: paginatorGlobalKey,
        pageLoadFuture: sendPagesDataRequest,
        pageItemsGetter: listItemsGetterPages,
        listItemBuilder: listItemBuilder,
        loadingWidgetBuilder: loadingWidgetMaker,
        errorWidgetBuilder: errorWidgetMaker,
        emptyListWidgetBuilder: emptyListWidgetMaker,
        totalItemsGetter: totalPagesGetter,
        pageErrorChecker: pageErrorChecker,
        scrollPhysics: BouncingScrollPhysics(),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context,
              contacts.where((e) => e.selected).cast<Contact>().toList());
        },
        label: Text('Proceed'),
        icon: Icon(Icons.arrow_forward),
        backgroundColor: app_color,
      ),
    );
  }

  Future<ContactsPaginator> sendPagesDataRequest(int page) async {
    print('page ${page}');
    try {
      Map response = await ContactRepo().fetchContacts(page.toString());
      ContactsPaginator pagesData =
          ContactsPaginator.fromJson(response["data"]);
      return pagesData;
    } catch (e) {
      print("check");
      print(e.toString());
      return ContactsPaginator.withError("Oops.. Fetching contacts failed");
    }
  }

  List<Contact> listItemsGetterPages(ContactsPaginator pagesData) {
    // return pagesData.products;
    pagesData.contacts.forEach((value) {
      contacts.add(value);
    });
    print("itemgetter");
    return contacts;
  }

  Widget listItemBuilder(dynamic contact, int index) {
    print('init' + index.toString());
    print(contact);
    return Column(
      children: [
        ListTile(
          leading: Checkbox(
            value: contact.selected,
            onChanged: (value) {
              setState(() {
                contact.selected = !contact.selected;
              });
            },
          ),
          title: Text(contact.phone ?? ''),
          subtitle: Text(contact.fullname()),
          trailing: Icon(Icons.more_horiz_outlined),
        ),
        Divider()
      ],
    );
  }

  Widget loadingWidgetMaker() {
    return Center(child: CircularProgressIndicator());
  }

  Widget errorWidgetMaker(ContactsPaginator pagesData, retryListener) {
    return Retry(
      buttontext: "Retry",
      icon: Icons.refresh,
      errormessage: pagesData.errorMessage ?? "No contacts Found",
      tryAgain: () {
        retryListener();
      },
    );
  }

  Widget emptyListWidgetMaker(ContactsPaginator pagesData) {
    return Center(
      child: Text(
        'No contacts found',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      ),
    );
  }

  int totalPagesGetter(ContactsPaginator pagesData) {
    print('lastpage:' + pagesData.lastPage.toString());
    return pagesData.total;
  }

  bool pageErrorChecker(ContactsPaginator pagesData) {
    if (pagesData.errorMessage != null) return true;
    if (pagesData.currentPage == 1 && pagesData.contacts.length == 0)
      return true;
    return false;
  }
}
