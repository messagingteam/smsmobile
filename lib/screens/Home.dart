import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/screens/Index.dart';
import 'package:eazisend/utils/env.dart';
import 'package:eazisend/widgets/Base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  Widget viewtoshow = Index();
  @override
  Widget build(BuildContext context) {
    return Base(
      body: viewtoshow,
      onBottomNavTap: (Widget widget) {
        setState(() {
          viewtoshow = widget;
        });
      },
    );
  }
}
