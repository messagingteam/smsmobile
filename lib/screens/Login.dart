import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: LoadingOverlay(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: ListView(
              children: [
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Align(
                      child: Image.asset(
                        'assets/images/login-register.png',
                        //width: 120,
                        //height: 80,
                      ),
                      alignment: AlignmentDirectional.center,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Center(
                      child: Text(
                    "Sign in",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )),
                ),
                TextFormField(
                  controller: emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email Address',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Enter your email';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Enter your password';
                    } else if (value.length <= 5) {
                      return 'The password must be at least 6 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 40),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: app_color_gradient),
                  ),
                  child: FlatButton(
                    color: Colors.transparent,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        login(context);
                      }
                    },
                    textColor: Colors.white,
                    child: Container(
                      height: 55,
                      padding: EdgeInsets.all(15),
                      child: Text(
                        "Login",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, register);
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'Don\'t have an account?',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: app_color)),
                            TextSpan(
                              text: ' Signup',
                              style: TextStyle(fontSize: 16, color: app_color),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'Forgot Password?',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: app_color)),
                            TextSpan(
                              text: ' Click Here',
                              style: TextStyle(fontSize: 16, color: app_color),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.black,
        opacity: 0.5,
      ),
    );
  }

  login(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    var loginResponse =
        await User().login(emailController.text, passwordController.text);
    print(loginResponse);
    setState(() {
      _isLoading = false;
    });
    if (loginResponse is User) {
      Provider.of<UserProvider>(context, listen: false).user = loginResponse;
      await loginResponse.save();
      token = loginResponse.token;
      Navigator.of(context).pushNamedAndRemoveUntil(home, (route) => false);
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(loginResponse["message"])));
    }
  }
}
