import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/CampaignProgress.dart';
import 'package:eazisend/widgets/Chart.dart';
import 'package:eazisend/widgets/HomeExploreMenu.dart';
import 'package:eazisend/widgets/HomeTopCard.dart';
import 'package:eazisend/widgets/Menu.dart';
import 'package:eazisend/widgets/SendMessageTo.dart';
import 'package:eazisend/widgets/Stats.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Index extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return IndexState();
  }
}

class IndexState extends State<Index> {
  String total_contacts = 'N/A';
  String total_groups = 'N/A';
  @override
  Widget build(BuildContext context) {
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: app_color,
        title: Text(
          "EaziSend",
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              showMenu(
                  context: context,
                  position: RelativeRect.fromLTRB(
                      MediaQuery.of(context).size.width, 100, 8, 0),
                  items: MenuList(context));
            },
            child: Icon(Icons.more_vert_outlined),
          )
        ],
      ),
      backgroundColor: Colors.grey[100],
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              HomeTopCard(
                username: provider.user.last_name,
                smsleft: FutureBuilder(
                  future: FetchAccountInfo(context, provider),
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data == null) {
                      return Text('N/A SMS left',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 23,
                              fontFamily: "Avenir",
                              color: Colors.grey[800]));
                    } else if (snapshot.hasData) {
                      print(snapshot.data["smsLeft"]);
                      return Text('${snapshot.data["smsLeft"]} SMS left',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 23,
                              fontFamily: "Avenir",
                              color: Colors.grey[800]));
                      ;
                    }
                    // By default, show a loading spinner
                    return Text('N/A SMS left',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 23,
                            fontFamily: "Avenir",
                            color: Colors.grey[800]));
                  },
                ),
                onPackagesTap: () {
                  Navigator.pushNamed(context, packages);
                },
                onTopupTap: () {
                  Navigator.pushNamed(context, topup);
                },
              ),
              SizedBox(
                height: 10,
              ),
              CampaignProgress(
                provider: provider,
              ),
              SizedBox(
                height: 10,
              ),
              SendMessageTo(
                menuClicked: null,
              ),
              SizedBox(
                height: 10,
              ),
              HomeExploreMenu(),
              SizedBox(
                height: 10,
              ),
              Stats(
                total_contacts: total_contacts,
                total_groups: total_groups,
              ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }

  updateTotalGroupsAndContacts(Map data) async {
    if (data != null)
      setState(() {
        this.total_groups = data["total_groups"].toString();
        this.total_contacts = data["total_contacts"].toString();
      });
  }

  FetchAccountInfo(BuildContext context, UserProvider provider) async {
    if (provider.accountinfo == null) {
      Map response = await provider.fetchAccountInfo();
      if (response["status"] == 1) {
        updateTotalGroupsAndContacts(response["data"]);
        return response["data"];
      }
      return null;
    }
    updateTotalGroupsAndContacts(provider.accountinfo);
    return provider.accountinfo;
  }
}
