import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/GroupsPaginator.dart';
import 'package:eazisend/resources/GroupRepo.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paginator/flutter_paginator.dart';

class SelectableGroupList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SelectableGroupListState();
  }
}

class SelectableGroupListState extends State<SelectableGroupList> {
  List<Group> groups = [];
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Choose Groups"),
        backgroundColor: app_color,
      ),
      body: Paginator.listView(
        key: paginatorGlobalKey,
        pageLoadFuture: sendPagesDataRequest,
        pageItemsGetter: listItemsGetterPages,
        listItemBuilder: listItemBuilder,
        loadingWidgetBuilder: loadingWidgetMaker,
        errorWidgetBuilder: errorWidgetMaker,
        emptyListWidgetBuilder: emptyListWidgetMaker,
        totalItemsGetter: totalPagesGetter,
        pageErrorChecker: pageErrorChecker,
        scrollPhysics: BouncingScrollPhysics(),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context,
              groups.where((e) => e.selectable).cast<Group>().toList());
        },
        label: Text('Proceed'),
        icon: Icon(Icons.arrow_forward),
        backgroundColor: app_color,
      ),
    );
  }

  Future<GroupsPaginator> sendPagesDataRequest(int page) async {
    print('page ${page}');
    // try {
    Map response = await GroupRepo().fetchGroups(page.toString());
    GroupsPaginator pagesData = GroupsPaginator.fromJson(response["data"]);
    return pagesData;
    // } catch (e) {
    //   print("check");
    //   print(e.toString());
    //   return GroupsPaginator.withError("Oops.. Fetching groups failed");
    // }
  }

  List<Group> listItemsGetterPages(GroupsPaginator pagesData) {
    // return pagesData.products;
    pagesData.groups.forEach((value) {
      groups.add(value);
    });
    print("itemgetter");
    return groups;
  }

  Widget listItemBuilder(dynamic group, int index) {
    print('init' + index.toString());
    return Column(
      children: [
        ListTile(
          leading: Checkbox(
            value: group.selectable,
            onChanged: (value) {
              setState(() {
                group.selectable = !group.selectable;
              });
            },
          ),
          title: Text(group.name ?? ''),
          subtitle: Text(group.description ?? ''),
          trailing: Icon(Icons.more_horiz_outlined),
        ),
        Divider()
      ],
    );
  }

  Widget loadingWidgetMaker() {
    return Center(child: CircularProgressIndicator());
  }

  Widget errorWidgetMaker(GroupsPaginator pagesData, retryListener) {
    return Retry(
      buttontext: "Retry",
      icon: Icons.refresh,
      errormessage: pagesData.errorMessage ?? "No groups found",
      tryAgain: () {
        retryListener();
      },
    );
  }

  Widget emptyListWidgetMaker(GroupsPaginator pagesData) {
    return Center(
      child: Text(
        'No group Found',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      ),
    );
  }

  int totalPagesGetter(GroupsPaginator pagesData) {
    print('lastpage:' + pagesData.lastPage.toString());
    return pagesData.total;
  }

  bool pageErrorChecker(GroupsPaginator pagesData) {
    if (pagesData.errorMessage != null) return true;
    if (pagesData.currentPage == 1 && pagesData.groups.length == 0) return true;
    return false;
  }
}
