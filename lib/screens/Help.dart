import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:url_launcher/url_launcher.dart';

class Help extends StatelessWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: app_color,
          title: Text("Help"),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Image.asset('assets/images/help.png'),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  "Boosu, Do you need any help.. Hit us up. We are 24hrs available",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Column(
                  children: [
                    for (var c in contacts)
                      GestureDetector(
                        onTap: () {
                          callNumber(c);
                        },
                        child: Card(
                          child: Container(
                            child: ListTile(
                              leading: Icon(CupertinoIcons.phone),
                              title: Text(
                                c,
                                style: TextStyle(fontSize: 18),
                              ),
                              trailing: Icon(CupertinoIcons.arrow_right_circle),
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  sendEmail('eazisend@gmail.com');
                },
                child: Card(
                  child: Container(
                    child: ListTile(
                      leading: Icon(CupertinoIcons.mail),
                      title: Text(
                        "eazisend@gmail.com",
                        style: TextStyle(fontSize: 18),
                      ),
                      trailing: Icon(CupertinoIcons.arrow_right_circle),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  sendEmail(String email) async {
    try {
      await FlutterEmailSender.send(Email(
        recipients: [email],
        body: 'Please type your message',
        subject: 'Enter your Subject',
      ));
    } on PlatformException catch (exception) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(exception.message),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  callNumber(String number) async {
    String url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Sorry, Calling ${number} Failed'),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }
}
