import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  createState() {
    return AboutState();
  }
}

class AboutState extends State<About> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About App"),
        centerTitle: true,
        backgroundColor: app_color,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/images/logo-full.png',
            width: 200,
          ),
          SizedBox(
            height: 15,
          ),
          SizedBox(height: 10),
          Center(
              child: Text(
            "Copyright 2020. All rights reserved.",
            // style: TextStyle(fontWeight: FontWeight.w700),
          )),
          SizedBox(
            height: 15,
          ),
          Center(
              child: Text(
            "Designed by codoxotechnologies.com",
            // style: TextStyle(fontWeight: FontWeight.w700),
          )),
        ],
      ),
    );
  }
}
