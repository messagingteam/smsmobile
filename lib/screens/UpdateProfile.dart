import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/resources/GroupRepo.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UpdateProfile extends StatefulWidget {
  final User user;
  UpdateProfile(this.user);
  @override
  State<StatefulWidget> createState() {
    return UpdateProfileState(this.user);
  }
}

class UpdateProfileState extends State<UpdateProfile> {
  final User user;
  UpdateProfileState(this.user);
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController othernamesController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    this.surnameController.text = user.last_name;
    this.othernamesController.text = user.other_names;
    this.emailController.text = user.email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 60, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/profile.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 2, left: 20),
                      child: Text(
                        "Update Profile",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: surnameController,
                        decoration: InputDecoration(
                          labelText: 'Surname',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter surname';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: othernamesController,
                        decoration: InputDecoration(
                          labelText: 'Othernames',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter othernames';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Email',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter email';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 40),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            UpdateProfile(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Update Profile",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          )
        ],
      ),
    );
  }

  UpdateProfile(BuildContext context) async {
    // final pr = await showLoading(context, 'Saving...');
    Map<String, dynamic> userdata = {
      "last_name": surnameController.text,
      "other_names": othernamesController.text,
      "email": emailController.text,
      "phone": user.phone
    };
    Map response = await AuthRepo().updateProfile(userdata);
    // await pr.hide();
    print(response);
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(response["message"])));
    if (response["status"] == 1) {
      Navigator.pop(context, userdata);
    }

    //Provider.of<UserProvider>(context, listen: false).addGroup(group);
  }
}
