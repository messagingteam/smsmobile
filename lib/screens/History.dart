import 'package:eazisend/widgets/MessageHistoryTile.dart';
import 'package:flutter/material.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';

class History extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HistoryState();
  }
}

class HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0,
            expandedHeight: 150.0,
            backgroundColor: app_color,
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              title: Row(
                children: [
                  Icon(
                    CupertinoIcons.bubble_right,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Messages History',
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  height: 30,
                  color: app_color,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child:
                        Center(child: Text("Swipe contact for more options")),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            topLeft: Radius.circular(40))),
                  ),
                ),
                Divider(),
                SizedBox(
                  height: 10,
                ),
                MessageHistoryTile(),
                MessageHistoryTile(),
                MessageHistoryTile(),
                MessageHistoryTile(),
                MessageHistoryTile(),
                MessageHistoryTile(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
