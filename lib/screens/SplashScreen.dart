import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  bool init = false;
  int seconds = 3;
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    checkIfTokenIsValid(context);
    return Scaffold(
      body: ConnectivityWidget(
        builder: (context, isOnline) => Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: app_color_gradient)),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.bubble_left_bubble_right_fill,
                  color: Colors.white,
                  size: 150,
                ),
                Text(
                  "Eazi Send",
                  style: TextStyle(
                      fontSize: 50,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Send It, Love It",
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 60,
                  child: LoadingIndicator(
                    indicatorType: Indicator.ballRotateChase,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  checkIfTokenIsValid(BuildContext context) async {
    if (!init) {
      init = true;
      Future.delayed(Duration(seconds: seconds), () async {
        Map response = await AuthRepo().checkTokenIfValid();
        print(response);
        if (response["status"] == -1 && response["code"] == '401') {
          await Navigator.pushReplacementNamed(context, login);
        } else if (response["status"] == -1 &&
            response["e"].toString().contains('401')) {
          await Navigator.pushReplacementNamed(context, login);
        } else if (response["status"] == 1) {
          Provider.of<UserProvider>(context, listen: false).user =
              User.Create(response["data"]);
          await Navigator.pushReplacementNamed(context, home);
        } else {
          seconds++;
          print("retrying");
          if (seconds <= 6) {
            init = false;
          }

          checkIfTokenIsValid(context);
        }
      });
    }
  }
}
