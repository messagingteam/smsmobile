import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }
}

class RegisterState extends State<Register> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController othernamesController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController sendernameController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 40, bottom: 10, left: 20),
                        child: Image.asset(
                          'assets/images/profile.png',
                          width: 120,
                          height: 120,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, left: 20),
                        child: Text(
                          "Create account",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: TextFormField(
                          controller: surnameController,
                          decoration: InputDecoration(
                            labelText: 'Surname',
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter your surname';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: TextFormField(
                          controller: othernamesController,
                          decoration: InputDecoration(
                            labelText: 'Othernames',
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter your othernames';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: TextFormField(
                          controller: phoneController,
                          keyboardType: TextInputType.numberWithOptions(),
                          decoration: InputDecoration(
                            labelText: 'Mobile Number',
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter your mobile number';
                            }
                            if (value.length != 10) {
                              return 'Mobile number should be 10 digits';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                            labelText: 'Email Address',
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter your email';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: TextFormField(
                          controller: sendernameController,
                          decoration: InputDecoration(
                            labelText: 'Sender Name',
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter your sender name';
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(height: 30),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: app_color_gradient),
                        ),
                        child: FlatButton(
                          color: Colors.transparent,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              register(context);
                            }
                          },
                          textColor: Colors.white,
                          child: Container(
                            height: 55,
                            padding: EdgeInsets.all(15),
                            child: Text(
                              "Register",
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacementNamed(context, login);
                        },
                        child: Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Center(
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                      text: 'Already having account?',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: app_color)),
                                  TextSpan(
                                    text: ' login',
                                    style: TextStyle(
                                        fontSize: 16, color: app_color),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              top: 30,
              left: 10,
            )
          ],
        ));
  }

  register(BuildContext context) async {
    Map response = await AuthRepo().register(
        last_name: surnameController.text,
        other_names: othernamesController.text,
        email: emailController.text,
        phone: phoneController.text,
        sender_name: sendernameController.text);
    if (response["status"] == 1) {
      Navigator.pushNamed(context, otp, arguments: {
        "token": response["token"],
        "phone": phoneController.text
      });
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(response["message"])));
    }
  }
}
