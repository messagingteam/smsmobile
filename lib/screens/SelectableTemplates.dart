import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:eazisend/models/Template.dart';
import 'package:provider/provider.dart';

class SelectableTemplate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SelectableTemplateState();
  }
}

class SelectableTemplateState extends State<SelectableTemplate> {
  String groupmessage = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose Template'),
        backgroundColor: app_color,
      ),
      body: Consumer<UserProvider>(
        builder: (context, notifier, _) {
          if (notifier.templates.isEmpty) {
            notifier.fetchTempates();
            print('fetching templates');
          }
          return ListView(
            children: [
              for (var i in notifier.templates)
                ExpansionTile(
                  leading: Radio(
                    value: i.message,
                    groupValue: groupmessage,
                    onChanged: (value) {
                      setState(() {
                        groupmessage = value;
                      });
                      // i.selectable = !value;
                      print(value);
                      //  Navigator.pop(context, value);
                    },
                  ),
                  title: Text(i.title),
                  children: <Widget>[
                    Container(
                      child: Text(i.message),
                      padding: EdgeInsets.all(15.0),
                    )
                  ],
                )
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context, groupmessage);
        },
        label: Text('Choose Selected'),
        icon: Icon(Icons.arrow_forward),
        backgroundColor: app_color,
      ),
    );
  }
}
