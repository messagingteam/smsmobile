import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountHistory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountHistoryState();
  }
}

class AccountHistoryState extends State<AccountHistory> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchPaymentHistory(context),
      builder: (context, snapshot) {
        print(snapshot);
        if (snapshot.hasData && snapshot.data.length == 0) {
          return Center(
            child: Text("No Data Available"),
          );
        } else if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView(
            children: [
              for (var i in snapshot.data)
                ListTile(
                  leading: Icon(CupertinoIcons.doc_on_doc),
                  title: Text(i["amount"]),
                  subtitle: Text('Status : ${i["status"]}'),
                )
            ],
          );
        }
        // By default, show a loading spinner
        return CircularProgressIndicator();
      },
    );
  }

  Future fetchPaymentHistory(BuildContext context) async {
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    if (provider.paymenthistory.isEmpty) {
      Map response = await provider.fetchPaymentHistory();
      if (response["status"] == 1) {
        //provider.paymenthistory = response["data"];
        return response["data"];
      }
      return [];
    }
    print("ss");
    return provider.paymenthistory;
  }
}
