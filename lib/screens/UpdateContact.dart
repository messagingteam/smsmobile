import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/ContactRepo.dart';
import 'package:eazisend/models/Contact.dart' as C;
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class UpdateContact extends StatefulWidget {
  final C.Contact contact;
  UpdateContact(this.contact);
  @override
  State<StatefulWidget> createState() {
    return UpdateContactState(this.contact);
  }
}

class UpdateContactState extends State<UpdateContact> {
  final C.Contact contact;
  UpdateContactState(this.contact);
  final _formKey = GlobalKey<FormState>();
  final format = DateFormat("yyyy-MM-dd");
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController othernameController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController dobController = TextEditingController();
  final TextEditingController groupController = TextEditingController(text: '');
  final TextEditingController groupNameController =
      TextEditingController(text: '');
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    phoneController.text = contact.phone;
    surnameController.text = contact.surname;
    othernameController.text = contact.othernames;
    titleController.text = contact.title;
    dobController.text = contact.dob;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 60, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/profile.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 2, left: 20),
                      child: Text(
                        "Update Contact",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: phoneController,
                        decoration: InputDecoration(
                          labelText: 'Mobile Number',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter your mobile number';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: surnameController,
                        decoration: InputDecoration(
                          labelText: 'Surname',
                        ),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: othernameController,
                        decoration: InputDecoration(
                          labelText: 'Othernames',
                        ),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                            labelText: 'Title of person',
                            hintText: "Eg. Mr, Mrs etc"),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: DateTimeField(
                        controller: dobController,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 12.0, horizontal: 8),
                          hintText: '',
                          labelText: 'Date of Birth',
                        ),
                        format: format,
                        onShowPicker: (context, currentValue) async {
                          return showDatePicker(
                              context: context,
                              firstDate: DateTime(1900),
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(2100));
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          labelText: 'Group',
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            UpdateContact(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Update Contact",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          )
        ],
      ),
    );
  }

  UpdateContact(BuildContext context) async {
//    final pr = await showLoading(context, 'Updating...');
    contact.phone = phoneController.text;
    contact.surname = surnameController.text;
    contact.othernames = othernameController.text;
    contact.title = titleController.text;
    contact.dob = dobController.text;
    Map response = await ContactRepo().updateContact(contact);
    //  await pr.hide();
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(response["message"])));
    if (response["status"] == 1) {
      Navigator.pop(context, contact);
    }
  }
}
