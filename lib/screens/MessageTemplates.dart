import 'package:eazisend/models/Template.dart';
import 'package:eazisend/resources/TemplateRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/actions.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/widgets/MessageTemplateTile.dart';
import 'package:eazisend/widgets/Retry.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:eazisend/providers/UserProvider.dart';

import 'UpdateTemplate.dart';

class MessageTemplates extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MessageTemplatesState();
  }
}

class MessageTemplatesState extends State<MessageTemplates> {
  bool isLoading = true;
  List<Template> templates = [];
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0,
            expandedHeight: 150.0,
            backgroundColor: app_color,
            actions: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, addnewtemplate);
                },
                icon: Icon(
                  CupertinoIcons.add_circled,
                  color: Colors.white,
                ),
              ),
            ],
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              title: Row(
                children: [
                  Icon(
                    CupertinoIcons.doc_on_doc,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Templates',
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  height: 30,
                  color: app_color,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Center(child: Text("")),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            topLeft: Radius.circular(40))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 250,
                  child: FutureProvider<List<Template>>(
                    initialData: [],
                    create: (context) => getTemplates(context),
                    child: Consumer<List<Template>>(
                      builder: (context, myModel, child) {
                        return ListView(
                          children: [
                            for (int i = 0; i < myModel.length; i++)
                              Column(
                                children: [
                                  MessageTemplateTile(
                                    myModel[i],
                                    i,
                                    onEditTap: () {
                                      editTemplate(context, myModel[i], i);
                                    },
                                    onDeleteTap: () {
                                      deleteTemplate(context, myModel[i], i);
                                    },
                                  ),
                                  Divider()
                                ],
                              )
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<List<Template>> getTemplates(BuildContext context) async {
    List<Template> gettemplates = [];
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    Map response = await TemplateRepo().fetchTemplate();
    if (provider.templates.isNotEmpty) {
      isLoading = false;
      gettemplates = provider.templates;
    } else {
      Map response = await TemplateRepo().fetchTemplate();
      if (response["status"] == 1) {
        response["data"].forEach((item) {
          gettemplates.add(Template.Create(item));
        });
        provider.templates = gettemplates;
        isLoading = false;
      }
    }

    return gettemplates;
  }

  deleteTemplate(BuildContext context, Template template, int index) async {
    SureToDelete(context, 'Alert', 'Are you sure you want to delete', () async {
      Map response = await TemplateRepo().deleteTemplate(template);
      print(response);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(response["message"] ?? ''),
        ),
      );
      if (response["status"] == 1) {
        setState(() {
          templates.removeAt(index);
        });
      }
    });
  }

  editTemplate(BuildContext context, Template template, int index) async {
    final response = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpdateTemplate(template),
      ),
    );
    if (response != null) {
      setState(() {
        template = response;
      });
    }
  }
}
