import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/SenderName.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/SendMessageRepo.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/screens/SelectableContactList.dart';
import 'package:eazisend/screens/SelectableGroupList.dart';
import 'package:eazisend/screens/SelectablePhoneContacts.dart';
import 'package:eazisend/screens/SelectableTemplates.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/utils/env.dart';
import 'package:eazisend/widgets/AppDropDown.dart';
//import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:eazisend/models/Contact.dart';
import 'package:page_slider/page_slider.dart';
import 'package:provider/provider.dart';

//we are going to use the following send types;
/*
instant
phone
contact
group
excel
photo
list
 */

class SendMessage extends StatefulWidget {
  final String sendtype;
  SendMessage({this.sendtype});
  @override
  State<StatefulWidget> createState() {
    return SendMessageState();
  }
}

class SendMessageState extends State<SendMessage> {
  GlobalKey<PageSliderState> _sliderKey = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final format = DateFormat("yyyy-MM-dd HH:mm");
  TextEditingController phoneController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  TextEditingController senderNameController = TextEditingController();
  TextEditingController scheduleController = TextEditingController();
  TextEditingController phoneControllerList = TextEditingController();
  bool sendinstant = true;
  List<Contact> phones = [];
  List<Contact> contacts = [];
  List<Group> groups = [];
  Map<String, dynamic> file = {"name": '', "path": ''};
  List<Map<String, String>> options = [
    // {"name": "Choose sender name", "value": "__select__"},
  ];
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SelectSenderName(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 40, bottom: 0, left: 20),
                      child: Image.asset(
                        'assets/images/login-register.png',
                        width: 120,
                        height: 120,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 0, bottom: 2, left: 20),
                      child: Text(
                        "Send Message",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 1, horizontal: 10),
                      child: TextFormField(
                        maxLines: 3,
                        controller: messageController,
                        decoration: InputDecoration(
                          labelText: 'Message',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Enter message";
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    GestureDetector(
                      onTap: () async {
                        final response = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SelectableTemplate(),
                          ),
                        );
                        if (response != null) {
                          messageController.text = response;
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Choose from templates',
                            style: TextStyle(fontSize: 16),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                        child: AppDropdown(
                          title: "Select Sender name",
                          options: options,
                          valueSelected: (result) {
                            print(result);
                            senderNameController.text = result;
                          },
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Checkbox(
                          value: sendinstant,
                          onChanged: (value) {
                            setState(() {
                              this.sendinstant = !this.sendinstant;
                            });
                          },
                        ),
                        Flexible(child: Text("Send Message Now")),
                      ],
                    ),
                    if (!sendinstant)
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                        child: DateTimeField(
                          controller: scheduleController,
                          validator: (value) {
                            if (value == null) {
                              return 'Choose Schedule Date/Time';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            isDense: true,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 12.0, horizontal: 8),
                            hintText: '',
                            labelText: 'Schedule Date/Time',
                          ),
                          format: format,
                          onShowPicker: (context, currentValue) async {
                            final date = await showDatePicker(
                                context: context,
                                firstDate: DateTime(1900),
                                initialDate: currentValue ?? DateTime.now(),
                                lastDate: DateTime(2100));
                            if (date != null) {
                              final time = await showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.fromDateTime(
                                    currentValue ?? DateTime.now()),
                              );
                              return DateTimeField.combine(date, time);
                            } else {
                              return currentValue;
                            }
                          },
                        ),
                      ),
                    SizedBox(height: 20),
                    WidgetToPickData(context, widget.sendtype)
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: MaterialButton(
                      color: app_color,
                      elevation: 0,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          BlastMessage(context);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.all(15.0),
                        child: Text(
                          "Send Message",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  BlastMessage(BuildContext context) async {
    if (senderNameController.text == '__select__' ||
        senderNameController.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Please choose sender name")));
      return;
    }
    if (!sendinstant && scheduleController.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Choose Schedule date")));
      return;
    }
    switch (widget.sendtype) {
      case 'instant':
        Map response = await SendMessageRepo().sendInstantMessage(
            phoneNumber: phoneController.text,
            message: messageController.text,
            senderName: senderNameController.text);
        print(response);
        if (response["status"] == 1) {
          messageController.text = '';
          phoneController.text = '';
        }
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
        break;
      case 'list':
        Map response = await SendMessageRepo().sendMessageWithOnlyContacts(
            phoneNumbers: phoneControllerList.text,
            scheduleDate: scheduleController.text,
            message: messageController.text,
            senderName: senderNameController.text);
        print(response);
        if (response["status"] == 1) {
          messageController.text = '';
          phoneControllerList.text = '';
        }
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
        break;
      case 'contact':
        if (contacts.isEmpty) {
          _scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text("Tap to choose contacts")));
          return;
        }
        Map response = await SendMessageRepo().sendMessageWithOnlyContacts(
            phoneNumbers: contacts.map((e) => e.phone).toList().join(','),
            scheduleDate: scheduleController.text,
            message: messageController.text,
            senderName: senderNameController.text);
        print(response);
        if (response["status"] == 1) {
          messageController.text = '';
          setState(() {
            contacts = [];
          });
        }
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
        break;
      case 'group':
        if (groups.isEmpty) {
          _scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text("Tap to choose groups")));
          return;
        }
        Map response = await SendMessageRepo().sendMessageWithOnlyGroup(
            groups: groups,
            scheduleDate: scheduleController.text,
            message: messageController.text,
            senderName: senderNameController.text);
        print(response);
        if (response["status"] == 1) {
          messageController.text = '';
          setState(() {
            groups = [];
          });
        }
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
        break;
      case 'phone':
        if (phones.isEmpty) {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(content: Text("Tap to choose phone contacts")));
          return;
        }
        Map response = await SendMessageRepo().sendMessageWithOnlyContacts(
            phoneNumbers: phones.map((e) => e.phone).toList().join(','),
            scheduleDate: scheduleController.text,
            message: messageController.text,
            senderName: senderNameController.text);
        print(response);
        if (response["status"] == 1) {
          messageController.text = '';
          setState(() {
            phones = [];
          });
        }
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text(response["message"])));
        break;
    }
  }

  Widget WidgetToPickData(BuildContext context, String sendtype) {
    Widget _widget;
    switch (sendtype) {
      case 'instant':
        _widget = Container(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: TextFormField(
            controller: phoneController,
            decoration: InputDecoration(
              labelText: 'Enter phone number',
            ),
            validator: (value) {
              if (value.length != 10) {
                return "Enter a valid phone number";
              }
              return null;
            },
          ),
        );
        break;
      case 'list':
        _widget = TextFormField(
          controller: phoneControllerList,
          maxLines: 3,
          decoration: InputDecoration(
            labelText: 'Enter phone numbers',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return "This field is required";
            }
            return null;
          },
        );
        break;
      case 'phone':
        _widget = GestureDetector(
          onTap: () async {
            final response = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SelectablePhoneContacts(),
              ),
            );
            if (response != null) {
              setState(() {
                phones = response;
              });
            }
          },
          child: Card(
            child: Container(
              height: 60,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.file_upload),
                      SizedBox(
                        width: 10,
                      ),
                      Text("Tap to Select Contacts")
                    ],
                  ),
                  if (phones.isNotEmpty)
                    Text('${phones.length} contacts selected')
                ],
              ),
            ),
          ),
        );
        break;
      case 'contact':
        _widget = GestureDetector(
          onTap: () async {
            final response = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SelectableContactList(),
              ),
            );
            if (response != null) {
              setState(() {
                contacts = response;
              });
            }
          },
          child: Card(
            child: Container(
              height: 60,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.file_upload),
                      SizedBox(
                        width: 10,
                      ),
                      Text("Tap to Select Contacts")
                    ],
                  ),
                  if (contacts.isNotEmpty)
                    Text('${contacts.length} contacts selected')
                ],
              ),
            ),
          ),
        );
        break;
      case 'group':
        _widget = GestureDetector(
          onTap: () async {
            final response = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SelectableGroupList(),
              ),
            );
            if (response != null) {
              setState(() {
                groups = response;
              });
            }
          },
          child: Card(
            child: Container(
              height: 60,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.file_upload),
                      SizedBox(
                        width: 10,
                      ),
                      Text("Tap to Select Group")
                    ],
                  ),
                  if (groups.isNotEmpty)
                    Text('${groups.length} groups selected')
                ],
              ),
            ),
          ),
        );
        break;
      case 'excel':
        _widget = GestureDetector(
          onTap: () {
            pickExcelFile();
          },
          child: Card(
            child: Container(
              height: 60,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.file_upload),
                      SizedBox(
                        width: 10,
                      ),
                      Text("Click to Choose excel file")
                    ],
                  ),
                  if (file["name"].isNotEmpty) Text('${file["name"]}')
                ],
              ),
            ),
          ),
        );
        break;
      default:
        _widget = TextFormField(
          controller: phoneControllerList,
          maxLines: 3,
          decoration: InputDecoration(
            labelText: 'Enter phone numbers',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return "This field is required";
            }
            return null;
          },
        );
        break;
    }
    return _widget;
  }

  Future<void> SelectSenderName(BuildContext context) async {
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    List<Map<String, String>> tempsender = [
      {"name": "Choose sender name", "value": "__select__"}
    ];
    if (provider.sendernames.isEmpty) {
      print("ok");
      // _scaffoldKey.currentState
      //     .showSnackBar(SnackBar(content: Text("Fetching sendernames")));
      Map response = await provider.fetchSenderNames();
      print(response);
      provider.sendernames.forEach((e) {
        tempsender.add({"name": e.sender_name, "value": e.sender_name});
      });
      if (tempsender.isNotEmpty) {
        setState(() {
          options = tempsender;
        });
      }
    } else {
      print("oks");
      provider.sendernames.forEach((e) {
        tempsender.add({"name": e.sender_name, "value": e.sender_name});
      });
      print(tempsender);
      if (tempsender.isNotEmpty) {
        setState(() {
          options = tempsender;
        });
      }
    }
  }

  Future<dynamic> pickExcelFile() async {
    // try {
    //   FilePickerResult results = await FilePicker.platform.pickFiles(
    //       type: FileType.custom,
    //       allowMultiple: false,
    //       allowedExtensions: ['xls', 'xlsx']);
    //   if (results != null) {
    //     file["path"] = results.paths[0].toString();
    //     file["name"] = results.names[0].toString();
    //   }
    // } on PlatformException catch (e) {
    //   _scaffoldKey.currentState.showSnackBar(
    //       SnackBar(content: Text("Unsupported operation" + e.toString())));
    // } catch (ex) {
    //   _scaffoldKey.currentState.showSnackBar(
    //       SnackBar(content: Text("Unsupported operation" + ex.toString())));
    // }
  }
}
