import 'package:eazisend/models/User.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/screens/UpdateProfile.dart';
import 'package:eazisend/widgets/SenderNames.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountDetailsState();
  }
}

class AccountDetailsState extends State<AccountDetails> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchuser(context),
      builder: (context, snapshot) {
        print(snapshot);
        if (snapshot.hasData && snapshot.data == null) {
          return Center(
            child: Text("User Details not found"),
          );
        } else if (snapshot.hasData && snapshot.data is User) {
          User user = snapshot.data;
          return ListView(
            children: [
              ListTile(
                title: Text("Full Name"),
                subtitle: Text(user.fullname()),
                leading: Icon(Icons.person),
                trailing: IconButton(
                  onPressed: () {
                    editProfile(context, user);
                  },
                  icon: Icon(Icons.edit),
                ),
              ),
              ListTile(
                title: Text("Email"),
                subtitle: Text(user.email),
                leading: Icon(Icons.email),
                trailing: IconButton(
                  onPressed: () {
                    editProfile(context, user);
                  },
                  icon: Icon(Icons.edit),
                ),
              ),
              ListTile(
                title: Text("Phone"),
                subtitle: Text(user.phone),
                leading: Icon(Icons.phone),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    Expanded(
                        child: Divider(
                      thickness: 3,
                    )),
                    SizedBox(
                      width: 20,
                    ),
                    Text("Sender Names")
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: SenderNames(),
              )
            ],
          );
        }
        // By default, show a loading spinner
        return CircularProgressIndicator();
      },
    );
  }

  editProfile(BuildContext context, User user) async {
    final response = Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpdateProfile(user),
      ),
    );
  }

  Future fetchuser(BuildContext context) async {
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    if (provider.currentuser == null) {
      Map response = await provider.fetchCurrentUserDetails();
      if (response["status"] == 1) {
        //provider.paymenthistory = response["data"];
        return User.Create(response["data"]);
      }
      return null;
    }
    return provider.currentuser;
  }
}
