import 'package:eazisend/models/Template.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/PackagesRepo.dart';
import 'package:eazisend/resources/TemplateRepo.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewTemplate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddNewTemplateState();
  }
}

class AddNewTemplateState extends State<AddNewTemplate> {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController messageController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<String> placeholders = [
    '[surname]',
    '[othernames]',
    '[fullname]',
    '[title]',
    '[dateofbirth]'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 60, bottom: 10, left: 20),
                      child: Image.asset(
                        'assets/images/login-register.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, bottom: 2, left: 20),
                      child: Text(
                        "Add New Template",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                          labelText: 'Title',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter title';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      child: TextFormField(
                        controller: messageController,
                        maxLines: 5,
                        decoration: InputDecoration(
                          labelText: 'Message Content',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter message content';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 40),
                    Text("Click to add Placeholders"),
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[100])),
                      child: Wrap(
                        children: [
                          for (var i in placeholders)
                            Container(
                              margin: EdgeInsets.all(5),
                              child: GestureDetector(
                                  onTap: () {
                                    messageController.text =
                                        messageController.text + i;
                                  },
                                  child: Chip(label: Text(i))),
                            )
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: FlatButton(
                        color: Colors.transparent,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            saveTemplate(context);
                          }
                        },
                        textColor: Colors.white,
                        child: Container(
                          height: 55,
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Save Template",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            top: 30,
            left: 10,
          )
        ],
      ),
    );
  }

  saveTemplate(BuildContext context) async {
    Template template =
        Template(message: messageController.text, title: titleController.text);
    Map response = await TemplateRepo().saveOrUpdate(template);
    // await pr.hide();
    print(response);
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(response["message"])));
    Provider.of<UserProvider>(context, listen: false).addToTemplate(template);
  }
}
