import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/Packages.dart';
import 'package:eazisend/models/SenderName.dart';
import 'package:eazisend/models/Template.dart';
import 'package:eazisend/models/User.dart';
import 'package:eazisend/resources/AuthRepo.dart';
import 'package:eazisend/resources/SenderNameRepo.dart';
import 'package:eazisend/resources/TemplateRepo.dart';
import 'package:flutter/foundation.dart';
import 'package:eazisend/models/Contact.dart' as C;

class UserProvider extends ChangeNotifier {
  User user;
  List<C.Contact> contacts = [];
  List<Group> groups = [];
  List<Packages> packages = [];
  List<Template> templates = [];
  List<SenderName> sendernames = [];
  List paymenthistory = [];
  bool packagesloaded = false;
  User currentuser;
  Map accountinfo;

  addContact(C.Contact contact) {
    contacts.insert(0, contact);
    notifyListeners();
  }

  addGroup(Group group) {
    groups.insert(0, group);
    notifyListeners();
  }

  addToTemplate(Template template) {
    templates.add(template);
    notifyListeners();
  }

  fetchSenderNames() async {
    Map response = await SenderNameRepo().fetchSenderNames();
    if (response["status"] == 1) {
      response["data"].forEach((e) {
        sendernames.add(SenderName.Create(e));
      });
    }
    return response;
  }

  fetchTempates() async {
    Map response = await TemplateRepo().fetchTemplate();
    if (response["status"] == 1) {
      response["data"].forEach((e) {
        templates.add(Template.Create(e));
      });
      notifyListeners();
    }
    return response;
  }

  fetchPaymentHistory() async {
    Map response = await AuthRepo().paymentHistory();
    if (response["status"] == 1) {
      paymenthistory = response["data"];
    }
    return response;
  }

  fetchCurrentUserDetails() async {
    Map response = await AuthRepo().checkTokenIfValid();
    if (response["status"] == 1) {
      currentuser = User.Create(response["data"]);
    }
    return response;
  }

  fetchAccountInfo() async {
    Map response = await AuthRepo().getAccountInfo();
    if (response["status"] == 1) {
      accountinfo = response["data"];
    }
    return response;
  }
}
