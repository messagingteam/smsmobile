import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

Future<void> OpenDialog(
    String title, String message, BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(message),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<ProgressDialog> showLoading(BuildContext context, String message) async {
  final ProgressDialog pr = ProgressDialog(
    context,
    type: ProgressDialogType.Normal,
    isDismissible: false,
  );
  pr.style(
      message: message,
      backgroundColor: Colors.white,
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut);
  await pr.show();
  return pr;
}

Future<void> closeLoading(ProgressDialog pr) async {
  await pr.hide();
}

SureToDelete(
    BuildContext context, String title, String message, Function onDelete) {
  return Alert(
    context: context,
    closeFunction: () {
      Navigator.pop(context);
    },
    title: title ?? "",
    desc: message ?? '',
    buttons: [
      DialogButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.red),
      DialogButton(
          color: Colors.green,
          child: Text(
            "Delete",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async {
            Navigator.pop(context);
            onDelete();
          }),
    ],
  ).show();
}

Confirm(BuildContext context, String title, String message, Function onConfirm,
    {String confirmbtntext = 'Yes', String cancelbtntext = 'Cancel'}) {
  return Alert(
    context: context,
    closeFunction: () {
      Navigator.pop(context);
    },
    title: title ?? "",
    desc: message ?? '',
    buttons: [
      DialogButton(
          child: Text(
            cancelbtntext,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.red),
      DialogButton(
          color: Colors.green,
          child: Text(
            confirmbtntext,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async {
            Navigator.pop(context);
            onConfirm();
          }),
    ],
  ).show();
}
