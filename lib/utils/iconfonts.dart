import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const IconData homeIcon = const IconData(0xFF,
    fontFamily: CupertinoIcons.iconFont,
    fontPackage: CupertinoIcons.iconFontPackage);
