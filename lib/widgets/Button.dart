import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final double btnradius;
  final double btnheight;
  final String btntext;
  final Function() onBtnPressed;
  Button(
      {@required this.btntext,
      this.btnheight = 55,
      this.btnradius = 50,
      @required this.onBtnPressed});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(btnradius)),
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: app_color_gradient),
      ),
      child: FlatButton(
        color: Colors.transparent,
        onPressed: onBtnPressed,
        textColor: Colors.white,
        child: Container(
          height: btnheight,
          padding: EdgeInsets.all(15),
          child: Text(
            btntext,
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
