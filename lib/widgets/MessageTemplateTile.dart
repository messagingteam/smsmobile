import 'package:eazisend/models/Template.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class MessageTemplateTile extends StatelessWidget {
  final Template template;
  final int index;
  final bool selectable;
  final Function() onEditTap;
  final Function() onDeleteTap;
  final Function(bool value) onChecked;
  MessageTemplateTile(this.template, this.index,
      {this.selectable = true,
      this.onDeleteTap,
      this.onEditTap,
      this.onChecked});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Container(
            color: Colors.white,
            child: ListTile(
              isThreeLine: true,
              leading: Icon(CupertinoIcons.doc_on_doc),
              title: Text(template.title ?? ''),
              subtitle: Text(template.message ?? ''),
              trailing: Icon(Icons.more_horiz_outlined),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
                caption: 'Edit',
                color: Colors.black45,
                icon: Icons.more_horiz,
                onTap: onEditTap),
            IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: onDeleteTap),
          ],
        ),
      ],
    );
  }
}
