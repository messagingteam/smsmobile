import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MessageHistoryTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      height: 150,
      width: 360,
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(top: 0, left: 12, right: 12, bottom: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Thu, Oct 22, 2020 1:08 PM",
                    style: TextStyle(fontSize: 18),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          bottomRight: Radius.circular(8),
                        ) // green shaped
                        ),
                    child: Text(
                      "Completed",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.person_3,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("No. of contacts : "),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.bubble_right,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Sent : ",
                    style: TextStyle(color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 2,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.bubble_right,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Failed : ",
                    style: TextStyle(color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
