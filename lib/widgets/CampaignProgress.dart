import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pusher_client/flutter_pusher.dart';
import 'package:laravel_echo/laravel_echo.dart';
import 'package:percent_indicator/percent_indicator.dart';

class CampaignProgress extends StatefulWidget {
  UserProvider provider;
  CampaignProgress({
    this.provider,
  });
  @override
  State<StatefulWidget> createState() {
    return CampaignProgressState();
  }
}

class CampaignProgressState extends State<CampaignProgress> {
  int total = 0;
  int pending = 0;
  int sent = 0;
  int failed = 0;
  @override
  void initState() {
    super.initState();
    setUpEcho(context);
  }

  @override
  Widget build(BuildContext context) {
    return campaign_id.isEmpty
        ? Container()
        : Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 20),
                  child: Text(
                    "Progress",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Divider(),
                Container(
                  height: 140,
                  padding: const EdgeInsets.all(10.0),
                  margin: EdgeInsets.all(10),
                  //decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Text("Pending"),
                            LinearPercentIndicator(
                              animation: true,
                              lineHeight: 10.0,
                              animationDuration: 2000,
                              percent: 0.9,
                              center: Text(
                                "",
                                style: TextStyle(fontSize: 12),
                              ),
                              linearStrokeCap: LinearStrokeCap.roundAll,
                              progressColor: Colors.orange,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text("Sent"),
                            new LinearPercentIndicator(
                              animation: true,
                              lineHeight: 10.0,
                              animationDuration: 2000,
                              percent: 0.9,
                              center: Text(
                                "",
                                style: TextStyle(fontSize: 12),
                              ),
                              linearStrokeCap: LinearStrokeCap.roundAll,
                              progressColor: Colors.greenAccent,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text("Failed"),
                            new LinearPercentIndicator(
                              animation: true,
                              lineHeight: 10.0,
                              animationDuration: 2000,
                              percent: 0.9,
                              center: Text(
                                "",
                                style: TextStyle(fontSize: 12),
                              ),
                              linearStrokeCap: LinearStrokeCap.roundAll,
                              progressColor: Colors.red,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 100,
                        width: 150,
                        // decoration:
                        //     BoxDecoration(border: Border.all(color: Colors.red)),
                        child: CircularPercentIndicator(
                          radius: 95.0,
                          lineWidth: 5.0,
                          percent: 1.0,
                          center: new Text("100%"),
                          progressColor: Colors.green,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
  }

  setUpEcho(BuildContext context) {
    print("am here oo");
    PusherOptions options = PusherOptions(
        host: sockethost,
        port: 6001,
        encrypted: false,
        auth: PusherAuth('http://${sockethost}/smsapi/public/broadcasting/auth',
            headers: {"Authorization": 'Bearer ' + token ?? ''}));

    PusherClient pusher =
        PusherClient('pusherKey', options, enableLogging: true);

    Echo echo = new Echo(
        {"broadcaster": 'pusher', 'client': pusher, 'key': 'pusherKey'});
    String channel =
        'App.User.${widget.provider.user.userId ?? 'ea35d76d-d811-4018-98a0-72c4b199a9b2'}';
    // String channel = '';
    print(channel);
    echo.private(channel).listen(
        'SMSCompletedEvent',
        (data) => {
              //{"userId":"ea35d76d-d811-4018-98a0-72c4b199a9b2","statistics":{"campaign_id":"12dc39a9-656e-42bb-8c7b-ae527eb00849","campaignDate":"Thu, Dec 3, 2020 1:55 PM","status":"processing","sent":0,"failed":0,"total":5,"pending":5,"message":{"token":"b54217fc-f959-4682-8674-a9046bcfc615","status":"pending"}}}
              // if(data.statistics.campaign_id===this.campaign_id){
              //   this.statistics = data.statistics
              // }
              print(data)
            });
  }
}
