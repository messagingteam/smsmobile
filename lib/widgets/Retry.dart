import 'package:flutter/material.dart';

class Retry extends StatelessWidget {
  final VoidCallback tryAgain;
  final IconData icon;
  final String buttontext;
  final String errormessage;
  final bool showbtn;

  Retry(
      {Key key,
      this.tryAgain,
      this.icon,
      this.buttontext = "",
      this.errormessage = "",
      this.showbtn = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: new Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 20.0,
            horizontal: 8.0,
          ),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  icon,
                  size: 80.0,
                  color: Colors.blue,
                ),
                new Text(
                  errormessage,
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                if (showbtn)
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      onPressed: tryAgain,
                      child: new Text(buttontext),
                      color: Colors.blue,
                      textColor: Colors.white,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
