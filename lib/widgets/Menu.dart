import 'package:eazisend/providers/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

final List menu = [
  {"icon": Icons.help, "name": "Help"},
  {"icon": Icons.feedback, "name": "Rate Us"},
  {"icon": Icons.info, "name": "About Us"},
  {"icon": Icons.exit_to_app, "name": "Logout"},
];

MenuList(BuildContext context) {
  List<PopupMenuEntry<int>> men = [];
  for (var i = 0; i < menu.length; i++) {
    men.add(
      PopupMenuItem<int>(
        child: ListTile(
          leading: Icon(menu[i]["icon"]),
          title: Text(menu[i]["name"]),
          onTap: () async {
            await Navigator.of(context).pop();
            await HandleMenuClick(context, i);
          },
        ),
      ),
    );
  }
  return men;
}

HandleMenuClick(context, result) async {
  switch (result) {
    case 0:
      Navigator.of(context).pushNamed(help);
      break;
    case 1:
      // Navigator.of(context).pushNamed(help);
      break;
    case 2:
      Navigator.of(context).pushNamed(about);
      break;
    case 3:
      await OpenDialog(context, () async {
        Provider.of<UserProvider>(context, listen: false).user = null;
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.remove("user");
        Navigator.of(context).pushNamedAndRemoveUntil(login, (route) => false);
      });
      break;
    default:
      // Navigator.of(context).pushNamed(profile);
      break;
  }
}

Future<void> OpenDialog(BuildContext context, Function onLogoutTap) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Info"),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("Are you sure you want to logout??"),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Logout'),
            onPressed: () async {
              await Navigator.of(context).pop();
              onLogoutTap();
            },
          ),
          FlatButton(
            child: Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
