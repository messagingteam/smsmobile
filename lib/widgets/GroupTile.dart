import 'package:eazisend/models/Group.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class GroupTile extends StatefulWidget {
  final Group group;
  final int index;
  final bool selectable;
  final Function() onEditTap;
  final Function() onDeleteTap;
  final Function(bool value) onChecked;
  GroupTile(this.group, this.index,
      {this.selectable = true,
      this.onDeleteTap,
      this.onEditTap,
      this.onChecked});
  @override
  State<StatefulWidget> createState() {
    return GroupTileState();
  }
}

class GroupTileState extends State<GroupTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Container(
            color: Colors.white,
            child: ListTile(
              leading: widget.selectable
                  ? Checkbox(
                      value: widget.group.selectable,
                      onChanged: (value) {
                        setState(() {
                          widget.group.selectable = !widget.group.selectable;
                        });
                        widget.onChecked(value);
                      },
                    )
                  : Icon(CupertinoIcons.doc_fill),
              title: Text(widget.group.name ?? ''),
              subtitle: Text(widget.group.description ?? ''),
              trailing: Icon(Icons.more_horiz_outlined),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
                caption: 'Edit',
                color: Colors.black45,
                icon: Icons.more_horiz,
                onTap: widget.onEditTap),
            IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: widget.onDeleteTap),
          ],
        ),
      ],
    );
  }
}
