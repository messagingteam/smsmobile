import 'package:flutter/material.dart';

class AppDropdown extends StatefulWidget {
  final valueSelected;
  final String title;
  final List<Map<String, dynamic>> options;
  final int indexSelected;
  AppDropdown(
      {this.title, this.options, this.valueSelected, this.indexSelected = 0});
  @override
  createState() {
    print(this.options);
    return AppDropdownState();
  }
}

class AppDropdownState extends State<AppDropdown> {
  String selected = '';
  //List<String> postype = ["Event", "Business"];
  //String selected = "Event";
  void initState() {
    super.initState();
    selected = widget.options.isNotEmpty
        ? widget.options[widget.indexSelected]["value"]
        : '__select__';
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      hint: Text(widget.title),
      value: selected,
      isExpanded: true,
      onChanged: (newValue) {
        widget.valueSelected(newValue);
        setState(() {
          this.selected = newValue;
        });
      },
      items: widget.options.isNotEmpty
          ? widget.options.map((item) {
              return DropdownMenuItem(
                child: new Text(item["name"]),
                value: item["value"],
              );
            }).toList()
          : [
              DropdownMenuItem(
                child: new Text("Choose sender name"),
                value: '__select__',
              )
            ],
    );
  }
}
