import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:eazisend/models/Contact.dart';

class ContactTile extends StatefulWidget {
  final Contact contact;
  final int index;
  final bool selectable;
  final Function() onEditTap;
  final Function() onDeleteTap;
  final Function() onAddToGroupTap;
  final Function(bool value) onChecked;
  ContactTile(this.contact, this.index,
      {this.selectable = true,
      this.onDeleteTap,
      this.onEditTap,
      this.onAddToGroupTap,
      this.onChecked});
  @override
  State<StatefulWidget> createState() {
    return ContactTileState();
  }
}

class ContactTileState extends State<ContactTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Container(
            margin: EdgeInsets.only(top: 3),
            color: Colors.white,
            child: ListTile(
              leading: widget.selectable
                  ? Checkbox(
                      value: widget.contact.selected,
                      onChanged: (value) {
                        setState(() {
                          widget.contact.selected = !widget.contact.selected;
                        });
                        widget.onChecked(value);
                      },
                    )
                  : Icon(CupertinoIcons.doc_fill),
              title: Text(widget.contact.phone ?? ''),
              subtitle: Text(widget.contact.fullname()),
              trailing: Icon(Icons.more_horiz_outlined),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
                caption: 'Add to group',
                color: Colors.blue,
                icon: Icons.archive,
                onTap: widget.onAddToGroupTap),
            IconSlideAction(
                caption: 'Edit',
                color: Colors.black45,
                icon: Icons.more_horiz,
                onTap: widget.onEditTap),
            IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: widget.onDeleteTap),
          ],
        ),
      ],
    );
  }
}
