import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';

class HomeTopCard extends StatelessWidget {
  final String username;
  final FutureBuilder smsleft;
  final Function() onTopupTap;
  final Function() onPackagesTap;
  HomeTopCard(
      {@required this.username,
      @required this.smsleft,
      this.onPackagesTap,
      this.onTopupTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFE8F1FF),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300],
            offset: Offset(0.0, 0.1), //(x,y)
            blurRadius: 0.5,
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      padding: EdgeInsets.only(bottom: 8),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('${username} Ernest',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 23,
                              fontFamily: "Avenir",
                              color: Colors.grey[800])),
                      SizedBox(
                        height: 10,
                      ),
                      smsleft,
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                Image.asset(
                  'assets/images/profile.png',
                  width: 100,
                )
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: onTopupTap,
                    child: Container(
                      height: 42,
                      padding: EdgeInsets.all(3),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: app_color_gradient),
                      ),
                      child: Center(
                        child: Text(
                          "Top up",
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: onPackagesTap,
                    child: Container(
                      height: 42,
                      padding: EdgeInsets.all(3),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(color: app_color),
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                      ),
                      child: Center(
                        child: Text(
                          "Packages",
                          style: TextStyle(fontSize: 19, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
