import 'package:flutter/material.dart';

class DropDown extends StatefulWidget {
  @override
  createState() {
    return DropDownState();
  }
}

class DropDownState extends State<DropDown> {
  String selected;
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
        value: selected,
        isExpanded: true,
        onChanged: (newValue) {},
        items: [
          DropdownMenuItem(
            child: Text(
              "Eazisend",
              style: TextStyle(color: Colors.black),
            ),
            value: 10,
          )
        ]);
  }
}
