import 'package:eazisend/routing/routes.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeExploreMenu extends StatelessWidget {
  final Function(String itemclicked) onMenuClick;
  HomeExploreMenu({this.onMenuClick});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          color: Colors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(
              "Explore other menus",
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.green,
                  fontWeight: FontWeight.w900),
            ),
          ),
          Flexible(
            child: GridView.count(
              childAspectRatio: 1.5,
              padding: const EdgeInsets.all(2),
              mainAxisSpacing: 30,
              crossAxisCount: 3,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, addnewcontact);
                  },
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            CupertinoIcons.plus,
                            size: 30,
                            color: app_color,
                          ),
                          padding: EdgeInsets.all(8),
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              border: Border.all(color: app_color, width: 2),
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Add new contact")
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, addnewgroup);
                  },
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            CupertinoIcons.person_2_square_stack,
                            size: 30,
                            color: app_color,
                          ),
                          padding: EdgeInsets.all(8),
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              border: Border.all(color: app_color, width: 2),
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Add new group")
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, history);
                  },
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            CupertinoIcons.doc_fill,
                            //size: 30,
                            color: app_color,
                          ),
                          padding: EdgeInsets.all(8),
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              border: Border.all(color: app_color, width: 2),
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Message History")
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, templates);
                  },
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            CupertinoIcons.doc_fill,
                            // size: 30,
                            color: app_color,
                          ),
                          padding: EdgeInsets.all(8),
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              border: Border.all(color: app_color, width: 2),
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Templates")
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Icon(
                          CupertinoIcons.app,
                          //size: 30,
                          color: app_color,
                        ),
                        padding: EdgeInsets.all(8),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            border: Border.all(color: app_color, width: 2),
                            borderRadius: BorderRadius.circular(50)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text("Apps")
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, help);
                  },
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            CupertinoIcons.question,
                            //   size: 30,
                            color: app_color,
                          ),
                          padding: EdgeInsets.all(8),
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              border: Border.all(color: app_color, width: 2),
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Help")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
