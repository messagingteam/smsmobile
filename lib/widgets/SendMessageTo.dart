import 'package:eazisend/routing/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SendMessageTo extends StatelessWidget {
  final Function(String typeclicked) menuClicked;
  SendMessageTo({@required this.menuClicked});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          color: Colors.white),
      child: Column(
        children: [
          ListTile(
            title: Text(
              "Quickly Send Message",
              style: TextStyle(
                  fontSize: 18, color: Colors.red, fontWeight: FontWeight.w900),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, sendmessage,
                  arguments: {"sendtype": 'list'});
            },
            title: Text("To New Contacts"),
            leading: Icon(CupertinoIcons.bubble_left),
            trailing: Icon(CupertinoIcons.chevron_forward),
          ),
          Divider(),
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, sendmessage,
                  arguments: {"sendtype": 'contact'});
            },
            title: Text("Saved Contacts"),
            leading: Icon(CupertinoIcons.person_3),
            trailing: Icon(CupertinoIcons.chevron_forward),
          ),
          Divider(),
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, sendmessage,
                  arguments: {"sendtype": 'group'});
            },
            title: Text("To Group"),
            leading: Icon(CupertinoIcons.person_2_square_stack),
            trailing: Icon(CupertinoIcons.chevron_forward),
          ),
          Divider(),
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, sendmessage,
                  arguments: {"sendtype": 'excel'});
            },
            title: Text("Predefined Messages"),
            leading: Icon(CupertinoIcons.doc_text),
            trailing: Icon(CupertinoIcons.chevron_forward),
          ),
        ],
      ),
    );
  }
}
