import 'package:eazisend/screens/Account.dart';
import 'package:eazisend/screens/Contacts.dart';
import 'package:eazisend/screens/Groups.dart';
import 'package:eazisend/screens/Index.dart';
import 'package:eazisend/screens/SelectableContactList.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:eazisend/routing/routes.dart';
import 'package:path/path.dart';

class Base extends StatefulWidget {
  final Widget appBar;
  final Widget body;
  final Key mykey;
  final Function(Widget widgettorender) onBottomNavTap;
  Base({this.appBar, this.body, this.mykey, this.onBottomNavTap});
  @override
  createState() {
    return BaseState();
  }
}

class BaseState extends State<Base> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //key: widget.mykey,
      // appBar: widget.appBar != null
      //     ? widget.appBar
      //     : AppBar(
      //         backgroundColor: app_color,
      //         title: Text("EaziSend"),
      //         actions: [Icon(Icons.more_vert_outlined)],
      //       ),
      extendBody: true,
      body: widget.body,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, sendto);
        },
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: app_color_gradient),
          ),
          child: Icon(
            Icons.message,
            color: Colors.white,
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                widget.onBottomNavTap(Index());
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 18, horizontal: 8),
                child: Icon(
                  CupertinoIcons.home,
                  color: app_color,
                  size: 30,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                widget.onBottomNavTap(Contacts());
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 18),
                child: Icon(
                  CupertinoIcons.person_2_fill,
                  color: app_color,
                  size: 30,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                widget.onBottomNavTap(Groups());
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Icon(
                  CupertinoIcons.square_list,
                  color: app_color,
                  size: 30,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                widget.onBottomNavTap(Account());
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  CupertinoIcons.person_alt_circle,
                  color: app_color,
                  size: 30,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
