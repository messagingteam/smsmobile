import 'package:eazisend/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Stats extends StatelessWidget {
  final String total_groups;
  final String total_contacts;
  Stats({this.total_contacts = '', this.total_groups = ''});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Card(
            child: Container(
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.person_3,
                    size: 40,
                    color: app_color,
                  ),
                  Text(
                    '${total_contacts}',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Contacts",
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: Card(
            child: Container(
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.person_2_square_stack,
                    size: 40,
                    color: app_color,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '${total_groups}',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Groups",
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
