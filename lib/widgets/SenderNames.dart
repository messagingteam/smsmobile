import 'package:eazisend/models/SenderName.dart';
import 'package:eazisend/providers/UserProvider.dart';
import 'package:eazisend/resources/SenderNameRepo.dart';
import 'package:eazisend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SenderNames extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SenderNamesState();
  }
}

class SenderNamesState extends State<SenderNames> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController sendernameController = TextEditingController();
  List<SenderName> sendernames = [];
  @override
  Widget build(BuildContext context) {
    UserProvider provider = Provider.of<UserProvider>(context, listen: false);
    if (provider.sendernames.isEmpty) {}
    return Wrap(
      children: [
        for (var i in provider.sendernames) Chip(label: Text(i.sender_name)),
        GestureDetector(
          onTap: () {
            addSenderName(context);
          },
          child: Container(
            margin: EdgeInsets.only(left: 15, top: 8),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                border: Border.all(color: app_color),
                borderRadius: BorderRadius.circular(30)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.add,
                  size: 20,
                ),
                Text(
                  "Add New",
                  style: TextStyle(fontSize: 12),
                ),
                SizedBox(
                  width: 10,
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  addSenderName(BuildContext context) {
    return Alert(
      context: context,
      closeFunction: () {
        // Navigator.pop(context);
      },
      title: "",
      desc: "Add new sendername",
      content: Form(
        key: _formKey,
        child: TextFormField(
          maxLength: 8,
          controller: sendernameController,
          decoration: InputDecoration(
            labelText: 'Sender Name',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Enter Sender name';
            }
            return null;
          },
        ),
      ),
      buttons: [
        DialogButton(
            child: Text(
              "Cancel",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            color: Colors.red),
        DialogButton(
          child: Text(
            "Add",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              Navigator.pop(context);
              saveSenderName(context);
            }
          },
          color: Colors.green,
        ),
      ],
    ).show();
  }

  saveSenderName(BuildContext context) async {
    Map response = await SenderNameRepo()
        .addNew(SenderName(sender_name: sendernameController.text));
    print(response);
  }

  fetchSenderNames(BuildContext context, UserProvider provider) async {
    Map response = await provider.fetchSenderNames();
    List<SenderName> _sendernames = [];
    if (response["status"] == 1) {
      response["data"].forEach((e) {
        _sendernames.add(SenderName(sender_name: e["sender_name"]));
      });
    }
    setState(() {
      sendernames = _sendernames;
    });
  }
}
