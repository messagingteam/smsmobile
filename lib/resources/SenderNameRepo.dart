import 'package:eazisend/models/SenderName.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'dart:async';
import 'dart:core';

class SenderNameRepo {
  final Api _api = new Api(baseurl);

  //routes for save or update template
  Future<Map<String, dynamic>> addNew(SenderName sendername) async {
    Map<String, dynamic> formdata = {
      "id": sendername.id ?? '',
      "senderName": sendername.sender_name
    };
    try {
      final Map response = await _api.post('/sender', formdata);
      print(response);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  fetch sender names
  Future<Map<String, dynamic>> fetchSenderNames() async {
    try {
      final Map response = await _api.get('/sender?approved=true');
      if (response["data"] != null) {
        return {"status": 1, "data": response["data"]};
      } else {
        String message = _api.getErrorMessage(response, msg: "Invalid Request");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to delete sender name
  Future<Map<String, dynamic>> delete(SenderName sendername) async {
    Map<String, dynamic> formdata = {
      "id": sendername.id,
      "sender_name": sendername.sender_name
    };
    try {
      final Map response = await _api.delete('/sender/${sendername.id}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
