import 'package:eazisend/models/Template.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'dart:async';
import 'dart:core';

class TemplateRepo {
  final Api _api = new Api(baseurl);

  //routes for save or update template
  Future<Map<String, dynamic>> saveOrUpdate(Template template) async {
    Map<String, dynamic> formdata = {
      "title": template.title,
      "message": template.message
    };
    try {
      final Map response = template.id != null
          ? await _api.put('/templates/${template.id}', formdata)
          : await _api.post('/templates', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  routes for fetching templates
  Future<Map<String, dynamic>> fetchTemplate() async {
    try {
      final List response = await _api.get('/templates');
      return {"status": 1, "message": "Successful", "data": response};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to delete template
  Future<Map<String, dynamic>> deleteTemplate(Template template) async {
    try {
      final Map response = await _api.delete('/templates/${template.id}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //  route to delete template
  Future<Map<String, dynamic>> deleteTemplates(List<Template> templates) async {
    List _templates = templates.map((e) => e.id).toList();
    try {
      final Map response =
          await _api.delete('/templates/bulk?templateIds=${_templates}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
