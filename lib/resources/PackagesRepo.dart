import 'package:eazisend/models/Packages.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'dart:async';
import 'dart:core';

class PackagesRepo {
  final Api _api = new Api(baseurl);

  //routes for save or update template
  Future<Map<String, dynamic>> fetchPackages() async {
    try {
      final Map response = await _api.get('/packages');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "data": response["data"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to delete sender name
  Future<Map<String, dynamic>> BuyPackage(Packages package) async {
    try {
      final Map response = await _api
          .post('/account/sms/buySmsFromPayment', {"packageId": package.id});
      if (response["data"] != null) {
        return {"status": 1, "data": response["data"]["redirectUrl"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  price enquiry
  Future<Map<String, dynamic>> priceEnquiry(
      {String method, String quantity}) async {
    Map formdata =
        method == 'price' ? {"price": quantity} : {"quantity": quantity};
    try {
      final Map response =
          await _api.post('/account/sms/pricingEnquiry', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "data": response["data"]};
      } else {
        String message = _api.getErrorMessage(response, msg: "Invalid Request");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//payment gateway link
  Future<Map<String, dynamic>> getPaymentLink({String amount}) async {
    try {
      final Map response =
          await _api.post('/account/sms/topUpBalance', {"amount": amount});
      if (response["data"] != null) {
        return {
          "status": 1,
          "data": response["data"]["redirectUrl"],
          "message": "Successful"
        };
      } else {
        String message = _api.getErrorMessage(response, msg: "Invalid Request");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
