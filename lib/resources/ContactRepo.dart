import 'package:eazisend/models/Contact.dart';
import 'package:eazisend/models/Group.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'dart:async';
import 'dart:core';

class ContactRepo {
  final Api _api = new Api(baseurl);

  //routes for saving contact
  Future<Map<String, dynamic>> save(Contact contact,
      {String group = '', String groupname = ''}) async {
    Map<String, dynamic> formdata = {
      "surname": contact.surname,
      "othernames": contact.othernames,
      "phone": contact.phone,
      "title": contact.title,
      "dob": contact.dob,
      "group": group,
      "groupname": groupname
    };
    try {
      final Map response = await _api.post('/contacts', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "Saving contact failed");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route for saving contacts with excel
  Future<Map<String, dynamic>> saveWithExcel(String filepath,
      {String group = '', String groupname = ''}) async {
    Map<String, dynamic> formdata = {
      "file": filepath,
      "group": group,
      "groupname": groupname
    };
    try {
      final Map response = await _api.postWithFile(
          '/contacts', 'file', formdata,
          headers: {'Content-type': 'multipart/form-data'});
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "Saving contact failed");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  routes to delete contact
  Future<Map<String, dynamic>> deleteContact(Contact contact) async {
    try {
      final Map response = await _api.delete('/contacts/${contact.id}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed deleting contact");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //  routes to delete contacts;
  Future<Map<String, dynamic>> deleteContacts(List<Contact> contacts) async {
    try {
      List _contacts = contacts.map((e) => e.id).toList();
      final Map response =
          await _api.delete('/contacts/bulk?contactIds=${_contacts}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed deleting contacts");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to update contact
  Future<Map<String, dynamic>> updateContact(Contact contact) async {
    Map<String, dynamic> formdata = {
      "surname": contact.surname,
      "othernames": contact.othernames,
      "phone": contact.phone,
      "title": contact.title,
      "dob": contact.dob,
    };
    try {
      final Map response = await _api.put('/contacts/${contact.id}', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed updating contact");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //add single contact to group
  Future<Map<String, dynamic>> addContactToGroup(
      Group group, Contact contact) async {
    List contacts = [contact.id];
    return await this.addContactsToGroup(group, contacts);
  }

// add contacts to group
  Future<Map<String, dynamic>> addContactsToGroup(
      Group group, List<Contact> contacts) async {
    List _contacts = contacts.map((e) => e.id).toList();
    try {
      final Map response = await _api
          .put('/groups/attachContacts/${group.id}', {"contactIds": _contacts});
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed updating contact");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //add single contact to group
  Future<Map<String, dynamic>> removeContactFromGroup(
      Group group, Contact contact) async {
    List contacts = [contact.id];
    return await this.removeContactsFromGroup(group, contacts);
  }

  // remove contacts to group
  Future<Map<String, dynamic>> removeContactsFromGroup(
      Group group, List<Contact> contacts) async {
    List _contacts = contacts.map((e) => e.id).toList();
    try {
      final Map response = await _api
          .delete('/groups/detachGroup/${group.id}?contactIds=${_contacts}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed removing contacts");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  // route to fetch contacts
  Future<Map<String, dynamic>> fetchContacts(String page) async {
    try {
      final Map response = await _api.get('/contacts?page=${page}');
      if (response["data"] != null) {
        return {"status": 1, "data": response, "message": "success"};
      } else {
        String message =
            _api.getErrorMessage(response, msg: "failed fetching contacts");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
