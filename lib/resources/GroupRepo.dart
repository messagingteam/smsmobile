import 'package:eazisend/models/Contact.dart';
import 'package:eazisend/models/Group.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:core';

class GroupRepo {
  final Api _api = new Api(baseurl);

  //routes for login
  Future<Map<String, dynamic>> save(Group group,
      {String phoneNumbers = '',
      List<Contact> contacts,
      String excelfile = ''}) async {
    Map<String, dynamic> formdata = {
      "name": group.name,
      "description": group.description,
      "createContacts":
          phoneNumbers.isEmpty || contacts != null || excelfile.isEmpty
              ? "true"
              : "false",
      "phoneNumbers": phoneNumbers,
      "contactIds": contacts != null
          ? contacts.map((e) => e.id).toList().toString()
          : [].toString(),
      "excelfile": excelfile
    };

    print(formdata);

    //try {
    final Map response = excelfile.isEmpty
        ? await _api.post('/groups', formdata)
        : await _api.postWithFile('/groups', 'excelfile', formdata);
    print(response);
    if (response["status"] != null && response["status"] == true) {
      return {
        "status": 1,
        "data": response["data"],
        "message": "Group added successfully"
      };
    } else {
      String message = "Invalid Credentials";
      return {"status": 0, "message": message};
    }
    /*} catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
      } */
  }

//  routes for deleting group
  Future<Map<String, dynamic>> deleteGroup(Group group) async {
    try {
      final Map<String, dynamic> response =
          await _api.delete('/groups/${group.id}');
      if (response["status"] != null && response["status"] == true) {
        //at this point. i just need the token
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //route for deleting groups
  Future<Map<String, dynamic>> deleteGroups(List<Group> groups) async {
    List _groups = groups.map((e) => e.id).toList();
    try {
      final Map<String, dynamic> response =
          await _api.delete('/groups/all?groupIds=${_groups}');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to set password
  Future<Map<String, dynamic>> updateGroup(Group group) async {
    Map<String, dynamic> formdata = {
      "name": group.name,
      "description": group.description,
    };
    print('${group.id}');
    try {
      final Map<String, dynamic> response =
          await _api.put('/groups/${group.id}', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "message": response["message"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //  route for fetching groups
  //returns paginated version of it
  Future<Map<String, dynamic>> fetchGroups(String page) async {
    try {
      final Map<String, dynamic> response =
          await _api.get('/groups?page=${page}');
      print(response);
      if (response["data"] != null) {
        return {"status": 1, "data": response, "message": "Successful"};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
