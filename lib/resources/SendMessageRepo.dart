import 'dart:convert';

import 'package:eazisend/models/Contact.dart';
import 'package:eazisend/models/Group.dart';
import 'package:eazisend/models/Template.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'dart:async';
import 'dart:core';

import 'package:flutter/cupertino.dart';

class SendMessageRepo {
  final Api _api = new Api(baseurl);

  //routes for save or update template
  Future<Map<String, dynamic>> sendMessageWithOnlyContacts({
    @required String scheduleDate,
    @required String message,
    @required senderName,
    String phoneNumbers,
    List<Contact> contacts,
  }) async {
    Map<String, dynamic> formdata = {
      "scheduleDate": scheduleDate,
      "message": message,
      "senderName": senderName,
    };
    if (contacts != null) {
      formdata["contactIds"] =
          contacts.map((e) => e.id).cast<String>().toList().toString();
    }
    if (phoneNumbers != null) {
      formdata["phoneNumbers"] = phoneNumbers;
    }
    print(formdata["contactIds"]);
    //try {
    final Map response =
        await _api.post('/sms/bulk/withOnlyContacts', formdata);
    if (response["campaignId"] != null) {
      return {
        "status": 1,
        "message": response["message"],
        "campaignId": response["campaignId"]
      };
    } else {
      String message = _api.getErrorMessage(response);
      return {"status": 0, "message": message};
    }
    // } catch (e) {
    //   return {
    //     "status": -1,
    //     "message": "Request failed... ${e}",
    //     "e": e,
    //   };
    // }
  }

  //  route for sending message with excel
  Future<Map<String, dynamic>> sendMessageWithOnlyExcel({
    @required String scheduleDate,
    @required String message,
    @required senderName,
    String file,
    List<Contact> contacts,
  }) async {
    Map<String, dynamic> formdata = {
      "scheduleDate": scheduleDate,
      "message": message,
      "senderName": senderName,
    };
    try {
      final Map response = await _api.postWithFile(
          '/sms/bulk/withOnlyExcel', 'file', formdata,
          headers: {'Content-type': 'multipart/form-data'});
      if (response["campaignId"] != null) {
        return {
          "status": 1,
          "message": response["message"],
          "campaignId": response["campaignId"]
        };
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //  route for sending instant message
  Future<Map<String, dynamic>> sendInstantMessage({
    @required String phoneNumber,
    @required String message,
    @required senderName,
  }) async {
    Map<String, dynamic> formdata = {
      "phoneNumber": phoneNumber,
      "message": message,
      "senderName": senderName,
    };
    try {
      final Map response = await _api.post('/sms/instant/send', formdata);
      if (response["status"] != null && response["status"] == true) {
        if (response['data']['status'] == 'sent') {
          return {"status": 1, "message": "Message sent successfully"};
        } else {
          return {
            "status": 1,
            "message": response["data"]["failed_reason"],
          };
        }
      } else {
        String message =
            _api.getErrorMessage(response, msg: "Sending message failed");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

  //routes for  template
  Future<Map<String, dynamic>> sendMessageWithOnlyGroup({
    @required String scheduleDate,
    @required String message,
    @required senderName,
    @required List<Group> groups,
  }) async {
    Map<String, dynamic> formdata = {
      "scheduleDate": scheduleDate,
      "message": message,
      "senderName": senderName,
      "groupIds": groups.map((e) => e.id).toList()
    };
    try {
      final Map response = await _api.post('/sms/bulk/withOnlyGroup', formdata);
      if (response["campaignId"] != null) {
        return {
          "status": 1,
          "message": response["message"],
          "campaignId": response["campaignId"]
        };
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
