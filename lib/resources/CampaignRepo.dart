import 'package:eazisend/models/Campaign.dart';
import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:core';

class CampaignRepo {
  final Api _api = new Api(baseurl);

  //routes for fetching campaigns
  Future<Map<String, dynamic>> fetchCampaigns() async {
    try {
      final Map response = await _api.get('/campaigns');
      return {"status": 1, "data": response ?? []};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route for fetching campaign details
  Future<Map<String, dynamic>> fetchCampaignDetail(Campaign campaign) async {
    try {
      final Map response =
          await _api.post('/campaigns', {"campaignId": campaign.campaignId});
      return {"status": 1, "data": response ?? []};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }
}
