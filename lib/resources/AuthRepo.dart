import 'package:eazisend/utils/api.dart';
import 'package:eazisend/utils/env.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:core';

class AuthRepo {
  final Api _api = new Api(baseurl);

  //routes for login
  Future<Map<String, dynamic>> login({
    @required String email,
    @required String password,
  }) async {
    Map<String, dynamic> formdata = {"email": email, "password": password};
    try {
      final Map response = await _api.post('/auth/login', formdata);
      print(response);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "data": response["data"]};
      } else {
        String message = "Invalid Credentials";
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  routes for registering user
  Future<Map<String, dynamic>> register({
    @required String last_name,
    @required String other_names,
    @required String email,
    @required String phone,
    @required String sender_name,
  }) async {
    Map<String, dynamic> formdata = {
      "last_name": last_name,
      "other_names": other_names,
      "email": email,
      "phone": phone,
      "sender_name": sender_name
    };
    try {
      final Map<String, dynamic> response =
          await _api.post('/auth/register', formdata);
      if (response["status"] != null && response["status"] == true) {
        //at this point. i just need the token
        return {"status": 1, "token": response["data"]["token"]};
      } else {
        String message = _api.getErrorMessage(response);
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  routes to verify otp
//  this when successful returns user details as it is for the login
  Future<Map<String, dynamic>> verifyOtp({
    @required String token,
    @required String verificationCode,
  }) async {
    Map<String, dynamic> formdata = {
      "token": token,
      "verificationCode": verificationCode,
    };
    try {
      final Map<String, dynamic> response =
          await _api.post('/auth/verify/otp', formdata);
      print(response);
      if (response["token"] != null) {
        return {"status": 1, "data": response};
      } else {
        String message = _api.getErrorMessage(response, msg: "Invalid Request");
        return {"status": 0, "message": message};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  route to set password
  Future<Map<String, dynamic>> setPassword({
    @required String password,
    @required String password_confirmation,
  }) async {
    Map<String, dynamic> formdata = {
      "password": password,
      "password_confirmation": password_confirmation,
    };
    try {
      final Map<String, dynamic> response =
          await _api.post('/auth/password', formdata);
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1};
      } else {
        return {"status": 0};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
      };
    }
  }

//  validatetoken
  Future<Map<String, dynamic>> checkTokenIfValid() async {
    try {
      final Map<String, dynamic> response = await _api.get('/user');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "data": response["data"]};
      } else {
        return {"status": 0};
      }
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
        "code": e.code().toString()
      };
    }
  }

//  route to resend token
  Future<Map<String, dynamic>> resendToken(String phone) async {
    try {
      final Map<String, dynamic> response =
          await _api.post('/auth/otp/resend', {"phoneNumber": phone});
      if (response["data"] != null) {
        return {
          "status": 1,
          "token": response["data"]["token"],
          "message": response["message"]
        };
      } else {
        return {"status": 0};
      }
    } catch (e) {
      print(e.code());
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
        "code": e.code()
      };
    }
  }

//  route to retrive payment history
  Future<Map<String, dynamic>> paymentHistory() async {
    try {
      final List response = await _api.get('/account/transactionHistory');
      return {"status": 1, "data": response};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
        "code": e.code()
      };
    }
  }

// route to get account info
  Future<Map<String, dynamic>> getAccountInfo() async {
    try {
      final Map response = await _api.get('/account/info');
      if (response["status"] != null && response["status"] == true) {
        return {"status": 1, "data": response["data"]};
      }
      return {"status": 0, "message": "Invalid request"};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
        "code": e.code()
      };
    }
  }

//  route for updating profile
  Future<Map<String, dynamic>> updateProfile(
      Map<String, dynamic> userdata) async {
    print(userdata);
    try {
      final Map response = await _api.put('/user', userdata);
      if (response["status"] != null && response["status"] == true) {
        return {
          "status": 1,
          "data": response["data"],
          "message": "Update successful"
        };
      }
      return {"status": 0, "message": "Invalid request"};
    } catch (e) {
      return {
        "status": -1,
        "message": "Request failed... ${e}",
        "e": e,
        "code": e.code()
      };
    }
  }
}
